# Code request

- [ ] Der Code Läuft ohne Errors und Warnings (Warnings, die mit weiteren Merges gelöst werden: -)
- [ ] Unit Tests wurden für den Code geschrieben
- [ ] Unit Tests sind durchgelaufen
- [ ] Definierte Code Guidelines und Standards wurden eingehalten
- [ ] Code dokumentiert
- [ ] Code wurde reviewed oder im Pair Programming erstellt
- [ ] Nicht funktionale Anforderungen erfüllt
