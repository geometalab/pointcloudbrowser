# Punktwolken-Browser

## Installation
Um den Punktwolken-Browser zu starten muss Docker und Docker-Compose installiert werden.

### Install Docker [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)
1. ```console
   $ sudo apt update
   ```
2. ```console
   $ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
   ```
3. ```console
   $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   ```
4. ```console
   $ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   ```
5. ```console
   $ sudo apt-get install docker-ce docker-ce-cli containerd.io
   ```

### Install Docker-Compose [https://docs.docker.com/compose/install/](https://docs.docker.com/engine/install/ubuntu/)

1. ```console
   $ sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   ```
2. ```console
   $ sudo chmod +x /usr/local/bin/docker-compose
   ```

## Start Pointcloudbrowser
1. ```console
   $ git clone https://gitlab.com/geometalab/pointcloudbrowser.git
   ```
2. ```console
   $ cd ./pointcloudbrowser
   ```
3. ```console
   $ docker-compose up --build
   ```

## User Manual

Point Cloud Browser is a web-based point cloud visualization and management software that allows you to not only visually configure your point clouds according to your needs, but also share them with other users. So that you don't have to keep the point clouds twice, they can be downloaded at any time. Thus, the Point Cloud Browser also serves as a management program. As a guest, all public point clouds can be viewed, while as a registered user, team point clouds and private ones can also be displayed. If a 2D view is needed, it can be calculated, visualized and downloaded directly with the tool.

<img src="images/overview.JPG" alt="Overview" width="600"/>

### Login and register

The login button is in the top right corner of the screen. By clicking on it, a window will appear where you can log in.

<img src="images/login.JPG" alt="Login form" width="400"/>

If you do not have an account yet, you can create one by clicking on "No account yet". The registration form will then open.

<img src="images/register.JPG" alt="Register form" width="400"/>

If you want to logout, click on your name in the top right corner.

<img src="images/logout.JPG" alt="Logout" width="400"/>

### Visualize point cloud

On the left side there is the selection list of point clouds. 

<img src="images/list2.JPG" alt="Point cloud list guest" width="200"/>

If you are not logged in, you will only see a selection of public point clouds. By clicking on an entry, the point cloud is displayed in the center of the screen.

If you are logged in, you will also see your private and shared point clouds.

<img src="images/list.JPG" alt="Point cloud list logged in" width="200"/>

For more space for your point cloud in the center of the screen, click on the arrow button near the list close it. You can later unfold the list if you wish to change the point cloud.

### Find the right point cloud

In the left list side panel you can search for keywords to find the right point cloud.

<img src="images/search.JPG" alt="Search point cloud" width="200"/>

#### Visualize in 3D

Once you have selected a point cloud, you can click on the 3D button on the right side of the screen to expand the settings.

<img src="images/3d.JPG" alt="3d options" width="200"/>

In the 3d options you can change the visualization, perform **measurements** or **download** the point cloud in the LAZ format.

If you are logged in, you will also find an **edit** button on your point clouds.

<img src="images/3dedit.JPG" alt="3d options edit" width="200"/>

If you click that edit button, you can edit all metadata or even upload a newer version of your point cloud. If you no longer need the point cloud, you can also **delete** it.

<img src="images/edit.JPG" alt="Edit form" width="400"/>

#### Visualize in 2D

Once you have selected a point cloud, you can click on the 2D button on the right side of the screen to expand the settings.

<img src="images/2d.JPG" alt="2D view" width="600"/>

There you can download the 2D GeoTIFF file.
