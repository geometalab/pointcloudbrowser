from django.db import models, transaction
from django.utils import timezone
from django.conf import settings
from users.models import User
from drf_chunked_upload.models import ChunkedUpload
import os

class UploadModel(ChunkedUpload):
    #This method is almost the same as the original https://github.com/jkeifer/drf-chunked-upload/blob/399faa228217b0633dec858afc72407185b81645/drf_chunked_upload/models.py - only the complete extension has changed
    @transaction.atomic
    def completed(self, completed_at=timezone.now(), ext=settings.COMPLETE_EXT):
        filename, file_extension = os.path.splitext(self.filename)
        ext = file_extension
        if ext != settings.INCOMPLETE_EXT:
            original_path = self.file.path
            self.file.name = os.path.splitext(self.file.name)[0] + ext
        self.status = self.COMPLETE
        self.completed_at = completed_at
        self.save()
        if ext != settings.INCOMPLETE_EXT:
            os.rename(
                original_path,
                os.path.splitext(self.file.path)[0] + ext,
            )

class EPTTempModel(models.Model):
    pointcloud_id = models.IntegerField(blank=False, null=False)
    last_used_date = models.DateTimeField(blank=True, null=False)
    is_currently_processing = models.BooleanField(default=False)
    save_path = models.CharField(max_length=250, blank=False, null=False)

class TIFTempModel(models.Model):
    pointcloud_id = models.IntegerField(blank=False, null=False)
    last_used_date = models.DateTimeField(blank=True, null=False)
    is_currently_processing = models.BooleanField(default=False)
    save_path = models.CharField(max_length=250, blank=False, null=False)

class Pointcloud(models.Model):
    class VisibilityMode(models.TextChoices):
        PRIVATE = 'private'
        PUBLIC = 'public'
        SELECTED_GROUP = 'group'

    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    place = models.CharField(max_length=100, blank=True)
    added_at = models.DateTimeField(auto_now_add=True)
    scan_date = models.DateField(blank=True, null=True)
    scan_device = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=800, blank=True)
    visibility = models.CharField(max_length=7, blank=False, choices=VisibilityMode.choices, default=VisibilityMode.PRIVATE)
    users_to_share_with = models.JSONField(blank=False, default=[])
    pointcloud_filepath = models.CharField(max_length=300, blank=False)
    metadata = models.JSONField(blank=True, null=True)