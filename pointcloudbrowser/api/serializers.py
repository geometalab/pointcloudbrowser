from rest_framework import serializers
from rest_framework.fields import HiddenField, CurrentUserDefault
from .models import Pointcloud, UploadModel, EPTTempModel, TIFTempModel

from users.serializers import UserSerializer
from users.models import User

from drf_chunked_upload.serializers import ChunkedUploadSerializer
from drf_chunked_upload.models import ChunkedUpload

import os
import uuid
import ntpath
import re

class UploadSerializer(ChunkedUploadSerializer):
    class Meta(ChunkedUploadSerializer.Meta):
        model = UploadModel

class PointcloudGetListSerializer(serializers.ModelSerializer):
    owner_name = serializers.SerializerMethodField()
    pointcloud_download_filepath = serializers.SerializerMethodField()

    def get_owner_name(self, obj):
        return obj.owner.username

    def get_pointcloud_download_filepath(self, obj):
        return '/static' + re.sub(r'^.*?static', '', obj.pointcloud_filepath)

    class Meta:
        model = Pointcloud
        fields = ('id', 'owner_name', 'name', 'place', 'added_at', 'scan_date', 'scan_device', 'description', 'visibility', 'users_to_share_with', 'pointcloud_download_filepath', 'metadata')

class PointcloudSerializer(serializers.ModelSerializer):
    owner = HiddenField(default=CurrentUserDefault())
    pointcloud_filepath = serializers.CharField(allow_null=True, default=None)
    scan_date = serializers.DateField(format="%Y-%m-%d", input_formats=['%Y-%m-%d', 'iso-8601'], allow_null=True, required=False)

    def to_internal_value(self, data):
        if 'scan_date' in data and data['scan_date'] == '':
            data['scan_date'] = None
        return super(PointcloudSerializer, self).to_internal_value(data)

    def create(self, validated_data):
        return Pointcloud.objects.create(**validated_data)

    class Meta:
        model = Pointcloud
        fields = '__all__'

class PointcloudUpdateRetrieveSerializer(serializers.ModelSerializer):
    owner_name = serializers.SerializerMethodField()

    def get_owner_name(self, obj):
        return obj.owner.username

    def rename_temp_model(self, temp_model, randomname):
        if temp_model is not None:
            headtemp, tailtemp = ntpath.split(temp_model.save_path)
            new_save_path = headtemp+'/'+randomname
            os.rename(temp_model.save_path, new_save_path)
            temp_model.save_path = new_save_path
            temp_model.save()

    def is_rename_needed(self, visibility_before, visibility_after):
        if visibility_before == Pointcloud.VisibilityMode.PUBLIC and visibility_after != Pointcloud.VisibilityMode.PUBLIC:
            return True
        if visibility_before == Pointcloud.VisibilityMode.SELECTED_GROUP and visibility_after == Pointcloud.VisibilityMode.PRIVATE:
            return True
        return False

    def rename_temp_cloud(self, instance):
        randomname = uuid.uuid4().hex
        head, tail = ntpath.split(instance.pointcloud_filepath)
        new_file_path = head+'/'+str(randomname)+'.laz'
        os.rename(instance.pointcloud_filepath, new_file_path)
        instance.pointcloud_filepath = new_file_path
        self.rename_temp_model(EPTTempModel.objects.filter(pointcloud_id=instance.id).first(), randomname)
        self.rename_temp_model(TIFTempModel.objects.filter(pointcloud_id=instance.id).first(), randomname+'.tif')

    def update(self, instance, validated_data):
        if self.is_rename_needed(instance.visibility, validated_data.get('visibility')):
            self.rename_temp_cloud(instance)
        elif 'users_to_share_with' in validated_data and validated_data.get('visibility') == Pointcloud.VisibilityMode.SELECTED_GROUP:
            missing = list(sorted(set(instance.users_to_share_with) - set(validated_data.get('users_to_share_with'))))
            if missing != []:
                self.rename_temp_cloud(instance)
        return super().update(instance, validated_data)

    class Meta:
        model = Pointcloud
        fields = ('id', 'owner_name', 'name', 'place', 'scan_date', 'scan_device', 'description', 'visibility', 'users_to_share_with')
