from django.test import TestCase

import requests
from rest_framework import status
from django.test.client import encode_multipart
from .models import UploadModel, Pointcloud
from users.models import User
import json
import os
import io
import shutil

class ApiTestUpload(TestCase):
    def setUp(self):
        self.login_url = '/login/users/'

        self.valid_user_creation = {
            "username" : "denis",
            "password": "secret1234",
            "email": "denis@gmail.com"
        }

        if not os.path.exists('/pointcloudbrowser/frontend/static/pointcloud-data/'):
            os.makedirs('/pointcloudbrowser/frontend/static/pointcloud-data/')
    
    def get_login_token(self):
        response_with_token = self.client.post(
            self.login_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )

        return 'JWT ' + response_with_token.data.get('token')

    def upload_file(self, file_path, filename, name):
        token = self.get_login_token()

        pointcloud = open(file_path, 'rb')
        total_size = os.path.getsize('./api/test-data/' + filename)

        slice_size = 1024 * 1024
        start = 0

        url = '/api/upload/'

        while total_size > start:
            if total_size - (start + slice_size) < 0:
                end = total_size 
            else:
                end = start + slice_size
            
            if(pointcloud.tell() + slice_size > total_size):
                chunk = pointcloud.read(total_size - pointcloud.tell())
            else:
                chunk = pointcloud.read(slice_size)
            chunk = io.BytesIO(chunk)
            contentrange = "bytes {0}-{1}/{2}".format(start, end-1, total_size)
            data = {'filename': filename,'file': chunk}
            content = encode_multipart('StrInGForBounDaRyyy--', data)
            content_type = 'multipart/form-data; boundary=StrInGForBounDaRyyy--'

            response = self.client.put(url, data=content, HTTP_AUTHORIZATION=token, HTTP_CONTENT_RANGE=contentrange, content_type=content_type)

            url = response.data.get('url')
            start = response.data.get('offset')
        
        return self.client.post(url, HTTP_AUTHORIZATION=token, data={'name': name})

    def clear_db(self):
        UploadModel.objects.all().delete()

    def test_upload_las(self):
        self.clear_db()
        response = self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'name': 'TestdataLAS'})
        self.assertEqual(1, Pointcloud.objects.all().count())
        self.assertEqual('private', Pointcloud.objects.all().first().visibility)
        self.assertEqual('TestdataLAS', Pointcloud.objects.all().first().name)

    def test_upload_e57(self):
        self.clear_db()
        response = self.upload_file('./api/test-data/Testfile.e57', 'Testfile.e57', 'TestdataE57')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'name': 'TestdataE57'})
        self.assertEqual(1, Pointcloud.objects.all().count())
        self.assertEqual('private', Pointcloud.objects.all().first().visibility)
        self.assertEqual('TestdataE57', Pointcloud.objects.all().first().name)

    def test_upload_ply(self):
        self.clear_db()
        response = self.upload_file('./api/test-data/Testfile.ply', 'Testfile.ply', 'TestdataPLY')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'name': 'TestdataPLY'})
        self.assertEqual(1, Pointcloud.objects.all().count())
        self.assertEqual('private', Pointcloud.objects.all().first().visibility)
        self.assertEqual('TestdataPLY', Pointcloud.objects.all().first().name)

    def test_upload_invalid(self):
        self.clear_db()
        response = self.upload_file('./api/test-data/Testfile.txt', 'Testfile.txt', 'Testdata')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(0, Pointcloud.objects.all().count())

    def test_unauthorized_login(self):
        pointcloud = open('./api/test-data/Testfile.las', 'rb')
        total_size = os.path.getsize('./api/test-data/' + 'Testfile.las')

        slice_size = 1024 * 1024
        start = 0

        url = '/api/upload/'

        if total_size - (start + slice_size) < 0:
            end = total_size 
        else:
            end = start + slice_size
           
        if(pointcloud.tell() + slice_size > total_size):
            chunk = pointcloud.read(total_size - pointcloud.tell())
        else:
            chunk = pointcloud.read(slice_size)
        chunk = io.BytesIO(chunk)
        contentrange = "bytes {0}-{1}/{2}".format(start, end-1, total_size)
        data = {'filename': 'TestdataLAS','file': chunk}
        content = encode_multipart('StrInGForBounDaRyyy--', data)
        content_type = 'multipart/form-data; boundary=StrInGForBounDaRyyy--'

        response = self.client.put(url, data=content, HTTP_CONTENT_RANGE=contentrange, content_type=content_type)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(0, Pointcloud.objects.all().count())

class ApiTestGetList(TestCase):
    def setUp(self):
        self.list_url = '/api/pointclouds/'
        self.login_url = '/login/token-auth/'

        self.user_1_login = {
            "username" : "denis",
            "password": "secret123"
        }

        self.user_2_login = {
            "username" : "denis2",
            "password": "secret123"
        }

        Pointcloud.objects.all().delete()
        User.objects.all().delete()
        user1 = User.objects.create(username='denis', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        user2 = User.objects.create(username='denis2', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        Pointcloud.objects.create(id=1, owner=user1, name='TestCloud1', description='bla', visibility='private')
        Pointcloud.objects.create(id=2, owner=user1, name='TestCloud2', visibility='public')
        Pointcloud.objects.create(id=3, owner=user2, name='TestCloud3', description='blu', visibility='public')
        Pointcloud.objects.create(id=4, owner=user2, name='TestCloud4', description='bla', visibility='public')
        Pointcloud.objects.create(id=5, owner=user2, name='TestCloud5', visibility='private')
        Pointcloud.objects.create(id=6, owner=user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])
        Pointcloud.objects.create(id=7, owner=user1, name='TestCloud7', description='bla', visibility='public')
        Pointcloud.objects.create(id=8, owner=user2, name='TestCloud8', description='bla', visibility='public')
        Pointcloud.objects.create(id=9, owner=user2, name='TestCloudblu', visibility='private')
        Pointcloud.objects.create(id=10, owner=user1, name='TestCloudbla', visibility='group', users_to_share_with=['denis2'])

    
    def get_token(self, login_user):
        response_with_token = self.client.post(
            self.login_url,
            data = json.dumps(login_user),
            content_type = 'application/json'
        )

        return 'JWT ' + response_with_token.data.get('token')

    def test_guest_all(self):
        response = self.client.get(
            self.list_url
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        self.assertEqual(response.data[0].get('name'), 'TestCloud2')
        self.assertEqual(response.data[1].get('name'), 'TestCloud3')
        self.assertEqual(response.data[2].get('name'), 'TestCloud4')
        self.assertEqual(response.data[3].get('name'), 'TestCloud7')
        self.assertEqual(response.data[4].get('name'), 'TestCloud8')

    def test_guest_search(self):
        response = self.client.get(
            self.list_url+'?search=bla'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0].get('name'), 'TestCloud4')
        self.assertEqual(response.data[1].get('name'), 'TestCloud7')
        self.assertEqual(response.data[2].get('name'), 'TestCloud8')

    def test_guest_search_empty(self):
        response = self.client.get(
            self.list_url+'?search=supercloud'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.data, [])

    def test_guest_page_1(self):
        response = self.client.get(
            self.list_url+'?page=1&pageSize=2'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0].get('name'), 'TestCloud2')
        self.assertEqual(response.data[1].get('name'), 'TestCloud3')

    def test_guest_page_2(self):
        response = self.client.get(
            self.list_url+'?page=2&pageSize=2'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0].get('name'), 'TestCloud4')
        self.assertEqual(response.data[1].get('name'), 'TestCloud7')

    def test_guest_page_3(self):
        response = self.client.get(
            self.list_url+'?page=3&pageSize=2'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('name'), 'TestCloud8')

    def test_guest_page_4(self):
        response = self.client.get(
            self.list_url+'?page=4&pageSize=2'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.data, [])

    def test_guest_search_page(self):
        response = self.client.get(
            self.list_url+'?page=1&pageSize=2&search=bla'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0].get('name'), 'TestCloud4')
        self.assertEqual(response.data[1].get('name'), 'TestCloud7')
    
    def test_guest_search_page_2(self):
        response = self.client.get(
            self.list_url+'?page=2&pageSize=2&search=bla'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('name'), 'TestCloud8')
    
    def test_user_1_all(self):
        token = self.get_token(self.user_1_login)
        response = self.client.get(
            self.list_url,
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 8)
        self.assertEqual(response.data[0].get('name'), 'TestCloud1')
        self.assertEqual(response.data[1].get('name'), 'TestCloud2')
        self.assertEqual(response.data[2].get('name'), 'TestCloud3')
        self.assertEqual(response.data[3].get('name'), 'TestCloud4')
        self.assertEqual(response.data[4].get('name'), 'TestCloud6')
        self.assertEqual(response.data[5].get('name'), 'TestCloud7')
        self.assertEqual(response.data[6].get('name'), 'TestCloud8')
        self.assertEqual(response.data[7].get('name'), 'TestCloudbla')

    def test_user_2_all(self):
        token = self.get_token(self.user_2_login)
        response = self.client.get(
            self.list_url,
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 9)
        self.assertEqual(response.data[0].get('name'), 'TestCloud2')
        self.assertEqual(response.data[1].get('name'), 'TestCloud3')
        self.assertEqual(response.data[2].get('name'), 'TestCloud4')
        self.assertEqual(response.data[3].get('name'), 'TestCloud5')
        self.assertEqual(response.data[4].get('name'), 'TestCloud6')
        self.assertEqual(response.data[5].get('name'), 'TestCloud7')
        self.assertEqual(response.data[6].get('name'), 'TestCloud8')
        self.assertEqual(response.data[7].get('name'), 'TestCloudblu')
        self.assertEqual(response.data[8].get('name'), 'TestCloudbla')

    def test_user_2_search(self):
        token = self.get_token(self.user_2_login)
        response = self.client.get(
            self.list_url+'?search=bla',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        self.assertEqual(response.data[0].get('name'), 'TestCloud4')
        self.assertEqual(response.data[1].get('name'), 'TestCloud6')
        self.assertEqual(response.data[2].get('name'), 'TestCloud7')
        self.assertEqual(response.data[3].get('name'), 'TestCloud8')
        self.assertEqual(response.data[4].get('name'), 'TestCloudbla')

    def test_single_get(self):
        token = self.get_token(self.user_2_login)
        response = self.client.get(
            self.list_url+'3/',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('id'), 3)

    def test_single_get_unauthorized_private(self):
        token = self.get_token(self.user_1_login)
        response = self.client.get(
            self.list_url+'3/',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data.get('detail'), "You do not have permission to perform this action.")

    def test_single_get_unauthorized_group(self):
        token = self.get_token(self.user_1_login)
        response = self.client.get(
            self.list_url+'6/',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data.get('detail'), "You do not have permission to perform this action.")

class ApiTestDelete(TestCase):
    def setUp(self):
        self.list_url = '/api/pointclouds/'
        self.login_url = '/login/token-auth/'

        self.user_1_login = {
            "username" : "denis",
            "password": "secret123"
        }

        self.user_2_login = {
            "username" : "denis2",
            "password": "secret123"
        }

        Pointcloud.objects.all().delete()
        User.objects.all().delete()
        user1 = User.objects.create(username='denis', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        user2 = User.objects.create(username='denis2', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        Pointcloud.objects.create(id=1, owner=user1, name='TestCloud1', description='bla', visibility='private')
        Pointcloud.objects.create(id=2, owner=user1, name='TestCloud2', visibility='public')
        Pointcloud.objects.create(id=3, owner=user2, name='TestCloud3', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])

    def get_token(self, login_user):
        response_with_token = self.client.post(
            self.login_url,
            data = json.dumps(login_user),
            content_type = 'application/json'
        )

        return 'JWT ' + response_with_token.data.get('token')

    def test_delete_as_owner(self):
        token = self.get_token(self.user_1_login)
        response = self.client.delete(
            self.list_url+'1/',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.filter(id=1).count(), Pointcloud.objects.none().count())

    def test_delete_as_group_owner(self):
        token = self.get_token(self.user_1_login)
        response = self.client.delete(
            self.list_url+'3/',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_delete_as_guest(self):
        response = self.client.delete(
            self.list_url+'2/',
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class ApiTestEdit(TestCase):
    def setUp(self):
        self.list_url = '/api/pointclouds/'
        self.login_url = '/login/token-auth/'

        self.user_1_login = {
            "username" : "denis",
            "password": "secret123"
        }

        self.user_2_login = {
            "username" : "denis2",
            "password": "secret123"
        }

        Pointcloud.objects.all().delete()
        User.objects.all().delete()
        user1 = User.objects.create(username='denis', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        user2 = User.objects.create(username='denis2', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        Pointcloud.objects.create(id=10, owner=user1, name='TestCloud1', description='bla', visibility='private')
        Pointcloud.objects.create(id=11, owner=user1, name='TestCloud2', visibility='public')
        Pointcloud.objects.create(id=12, owner=user2, name='TestCloud5', visibility='private')
        Pointcloud.objects.create(id=13, owner=user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])

    def get_token(self, login_user):
        response_with_token = self.client.post(
            self.login_url,
            data = json.dumps(login_user),
            content_type = 'application/json'
        )

        return 'JWT ' + response_with_token.data.get('token')

    def test_guest_edit(self):
        data = {'name': 'NewNameTestCloud2'}
        response = self.client.patch(
            self.list_url+'11/',
            data=data,
            content_type = 'application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Pointcloud.objects.get(id=11).name, "TestCloud2")

    def test_group_edit(self):
        token = self.get_token(self.user_1_login)
        data = {'name': 'NewNameTestCloud6'}
        response = self.client.patch(
            self.list_url+'13/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Pointcloud.objects.get(id=13).name, "TestCloud6")
    
    def test_user_1_edit(self):
        token = self.get_token(self.user_1_login)
        data = {'name': 'NewNameTestCloud1'}
        response = self.client.patch(
            self.list_url+'10/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.get(id=10).name, "NewNameTestCloud1")

    def test_user_2_edit(self):
        token = self.get_token(self.user_2_login)
        data = {'name': 'NewNameTestCloud5', 'visibility': 'public', 'scan_device': 'Handy', 'pointcloud_filepath': './should/not/be/possible/to/change'}
        response = self.client.patch(
            self.list_url+'12/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        edited_pointcloud = Pointcloud.objects.get(id=12)
        self.assertEqual(edited_pointcloud.name, "NewNameTestCloud5")
        self.assertEqual(edited_pointcloud.visibility, "public")
        self.assertEqual(edited_pointcloud.scan_device, "Handy")
        self.assertEqual(edited_pointcloud.pointcloud_filepath, "")

class ApiTestEditPointcloudFile(TestCase):
    def setUp(self):
        self.list_url = '/api/pointclouds/'
        self.login_url = '/login/token-auth/'

        self.user_1_login = {
            "username" : "denis",
            "password": "secret123"
        }

        self.user_2_login = {
            "username" : "denis2",
            "password": "secret123"
        }

        Pointcloud.objects.all().delete()
        User.objects.all().delete()
        self.user1 = User.objects.create(username='denis', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')
        self.user2 = User.objects.create(username='denis2', password='pbkdf2_sha256$260000$PcX3aeBwkzwgjvCQhHOs5j$mNQplc/ZDcJFLHddH/u4FM8T08odvaY3GpABWkRcjEo=', email='denis@gmail.com')

    
    def get_token(self, login_user):
        response_with_token = self.client.post(
            self.login_url,
            data = json.dumps(login_user),
            content_type = 'application/json'
        )

        return 'JWT ' + response_with_token.data.get('token')

    def upload_file(self, file_path, filename, name, user, old_id):
        token = self.get_token(user)

        pointcloud = open(file_path, 'rb')
        total_size = os.path.getsize('./api/test-data/' + filename)

        slice_size = 1024 * 1024
        start = 0

        url = '/api/upload/'

        while total_size > start:
            if total_size - (start + slice_size) < 0:
                end = total_size 
            else:
                end = start + slice_size
            
            if(pointcloud.tell() + slice_size > total_size):
                chunk = pointcloud.read(total_size - pointcloud.tell())
            else:
                chunk = pointcloud.read(slice_size)
            chunk = io.BytesIO(chunk)
            contentrange = "bytes {0}-{1}/{2}".format(start, end-1, total_size)
            data = {'filename': filename,'file': chunk}
            content = encode_multipart('StrInGForBounDaRyyy--', data)
            content_type = 'multipart/form-data; boundary=StrInGForBounDaRyyy--'

            response = self.client.put(url, data=content, HTTP_AUTHORIZATION=token, HTTP_CONTENT_RANGE=contentrange, content_type=content_type)

            url = response.data.get('url')
            start = response.data.get('offset')
        
        return self.client.post(url, HTTP_AUTHORIZATION=token, data={'name': name, 'old_pointcloud_id': old_id})

    def clear_db(self):
        UploadModel.objects.all().delete()

    def test_edit_new_file(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=100, owner=self.user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])
        self.clear_db()

        self.assertEqual(Pointcloud.objects.all().count(), 1)

        response = self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_1_login, Pointcloud.objects.all().first().id)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Pointcloud.objects.all().count(), 2)
        self.assertEqual('private', Pointcloud.objects.get(id=1).visibility)
        self.assertEqual('TestdataLAS', Pointcloud.objects.get(id=1).name)
    
    def test_edit_new_file_unauthorized(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=101, owner=self.user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])
        self.clear_db()

        self.assertEqual(Pointcloud.objects.all().count(), 1)

        response = self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_1_login, Pointcloud.objects.all().first().id)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Pointcloud.objects.all().count(), 2)
    
    def test_userstoshare_edit_group_group(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=102, owner=self.user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])

        token = self.get_token(self.user_2_login)
        self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_2_login, 102)

        data = {'name': 'RemoveTestPC', 'visibility': 'group', 'users_to_share_with': ["denis", "walter"]}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).name, "RemoveTestPC")
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).users_to_share_with, ["denis", "walter"])

        pointcloud_path = Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).pointcloud_filepath

        data = {'name': 'RemoveTestPCEdit', 'users_to_share_with': ["denis"], 'visibility': 'group'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.all().first().name, "RemoveTestPCEdit")
        self.assertEqual(Pointcloud.objects.all().first().users_to_share_with, ["denis"])
        self.assertNotEqual(Pointcloud.objects.all().first().pointcloud_filepath, pointcloud_path)

    def test_userstoshare_edit_public_group(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=105, owner=self.user2, name='TestCloud64', description='bla', visibility='public')

        token = self.get_token(self.user_2_login)
        self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_2_login, 105)

        data = {'name': 'RemoveTestPC', 'visibility': 'public'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).name, "RemoveTestPC")
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).visibility, "public")

        pointcloud_path = Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).pointcloud_filepath

        data = {'name': 'RemoveTestPCEdit', 'users_to_share_with': ["denis"], 'visibility': 'group'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.all().first().name, "RemoveTestPCEdit")
        self.assertEqual(Pointcloud.objects.all().first().users_to_share_with, ["denis"])
        self.assertNotEqual(Pointcloud.objects.all().first().pointcloud_filepath, pointcloud_path)

    def test_userstoshare_edit_public_private(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=107, owner=self.user2, name='TestCloud64', description='bla', visibility='public')

        token = self.get_token(self.user_2_login)
        self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_2_login, 107)

        data = {'name': 'RemoveTestPC', 'visibility': 'public'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).name, "RemoveTestPC")
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).visibility, "public")

        pointcloud_path = Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).pointcloud_filepath

        data = {'name': 'RemoveTestPCEdit', 'visibility': 'private'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.all().first().name, "RemoveTestPCEdit")
        self.assertEqual(Pointcloud.objects.all().first().visibility, "private")
        self.assertNotEqual(Pointcloud.objects.all().first().pointcloud_filepath, pointcloud_path)

    def test_userstoshare_edit_group_private(self):
        Pointcloud.objects.all().delete()
        Pointcloud.objects.create(id=110, owner=self.user2, name='TestCloud6', description='bla', visibility='group', users_to_share_with=['denis', 'walter'])

        token = self.get_token(self.user_2_login)
        self.upload_file('./api/test-data/Testfile.las', 'Testfile.las', 'TestdataLAS', self.user_2_login, 110)

        data = {'name': 'RemoveTestPC', 'visibility': 'group', 'users_to_share_with': ["denis", "walter"]}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).name, "RemoveTestPC")
        self.assertEqual(Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).users_to_share_with, ["denis", "walter"])

        pointcloud_path = Pointcloud.objects.get(id=Pointcloud.objects.all().first().id).pointcloud_filepath

        data = {'name': 'RemoveTestPCEdit', 'visibility': 'private'}
        response = self.client.patch(
            self.list_url+str(Pointcloud.objects.all().first().id)+'/',
            data=data,
            content_type = 'application/json',
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Pointcloud.objects.all().first().name, "RemoveTestPCEdit")
        self.assertEqual(Pointcloud.objects.all().first().visibility, "private")
        self.assertNotEqual(Pointcloud.objects.all().first().pointcloud_filepath, pointcloud_path)