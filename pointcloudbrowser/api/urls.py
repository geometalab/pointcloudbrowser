from django.urls import include, path, re_path
from .views import UploadView, get_pointcloud_3d, get_pointcloud_2d, PointcloudViewset

#from https://github.com/m3hrdadfi/drf-chunked-upload/blob/master/drf_chunked_upload/urls.py
UUID = r'[a-fA-F0-9]{{8}}-' + \
       r'[a-fA-F0-9]{{4}}-' + \
       r'[a-fA-F0-9]{{4}}-' + \
       r'[a-fA-F0-9]{{4}}-' + \
       r'[a-fA-F0-9]{{12}}'
ID_QUERY = r'(?P<{id}>{uuid})'.format(uuid=UUID, id='{id}')
PK_QUERY = ID_QUERY.format(id='pk')

pointcloud_list = PointcloudViewset.as_view({
    'get': 'list'
})

pointcloud_detail = PointcloudViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    path('pointclouds/<int:pointcloud_id>/view3d/', get_pointcloud_3d),
    path('pointclouds/<int:pointcloud_id>/view2d/', get_pointcloud_2d),
    
    path(r'pointclouds/', pointcloud_list, name='pointcloud-list'),
    re_path(r'pointclouds/(?P<pk>\d+)/$', pointcloud_detail, name='pointcloud-detail'),
    re_path(r'pointclouds/(?P<page>.+)(?P<pageSize>.+)(?P<search>.+)/$', pointcloud_list, name='pointcloud-filtered-list'),

    re_path(r'^upload/$', UploadView.as_view(), name='chunkedupload-list'),
    re_path(r'^upload/{}/$'.format(PK_QUERY), UploadView.as_view(), name='chunkedupload-detail'),
]