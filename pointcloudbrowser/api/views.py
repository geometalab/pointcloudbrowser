from django.shortcuts import get_object_or_404
from django.core import serializers
from django.conf import settings
from django.db.models import Q

from rest_framework.decorators import api_view
from rest_framework import permissions, status
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework import viewsets

from drf_chunked_upload.views import ChunkedUploadView
from drf_chunked_upload.models import ChunkedUpload
from drf_chunked_upload.exceptions import ChunkedUploadError

from users.models import User
from .models import Pointcloud, UploadModel, EPTTempModel, TIFTempModel
from .serializers import UploadSerializer, PointcloudSerializer, PointcloudGetListSerializer, PointcloudUpdateRetrieveSerializer

from pointcloudpipeline import pipeline_worker

import os
import shutil
import time
from datetime import datetime, timezone
from pathlib import Path

class UploadView(ChunkedUploadView):
    model = UploadModel
    serializer_class = UploadSerializer
    do_md5_check = False
    do_checksum_check = False
    http_method_names = ['put', 'post']

    def delete_empty_folders(self, path):
        for root, dirnames, filenames in os.walk(path, topdown=False):
            for dirname in dirnames:
                if len(os.listdir(os.path.realpath(os.path.join(root, dirname)))) == 0:
                    os.rmdir(os.path.realpath(os.path.join(root, dirname)))

    def cleanup(self, chunked_upload):
        chunked_upload.delete()
        self.delete_empty_folders('pointcloud-upload/chunked_uploads')

    def on_completion(self, chunked_upload, request):
        if not (str(chunked_upload.file.path).endswith('.laz') or str(chunked_upload.file.path).endswith('.las') or str(chunked_upload.file.path).endswith('.e57') or str(chunked_upload.file.path).endswith('.ply')):
            return Response({"detail": "You've uploaded an unsupported filetype. Supported are: laz, las, e57 and ply"}, status=status.HTTP_400_BAD_REQUEST)

        metadata = ""
        try:
            destination_folder_path = "frontend/static/pointcloud-data/"+str(request.user.id)+'_'+str(request.user)

            Path(destination_folder_path).mkdir(parents=True, exist_ok=True)
            metadata = pipeline_worker.convert_to_laz(chunked_upload.file.path, destination_folder_path)
        except:
            self.cleanup(chunked_upload)
            return Response({"detail": "Your poincloud is invalid or corrupted."}, status=status.HTTP_400_BAD_REQUEST)

        pointcloud_serializer = PointcloudSerializer(context={'request': request}, data=request.data)
        if pointcloud_serializer.is_valid():
            pointcloud_serializer.save(pointcloud_filepath=destination_folder_path+'/'+str(chunked_upload.id)+'.laz', metadata=metadata)
            self.cleanup(chunked_upload)

            if 'old_pointcloud_id' in request.data:
                old_pointcloud = Pointcloud.objects.get(id=request.data.get('old_pointcloud_id'))
                if old_pointcloud.owner.id == request.user.id:
                    delete_pointcloud(old_pointcloud.pointcloud_filepath, old_pointcloud.id)
                    old_pointcloud.delete()

            return Response(request.data, status=status.HTTP_201_CREATED)

        self.cleanup(chunked_upload)
        return Response({"detail": "Invalid Pointcloud Information."}, status=status.HTTP_400_BAD_REQUEST)
    
    #This post-method is almost the same as the original https://github.com/jkeifer/drf-chunked-upload/blob/master/drf_chunked_upload/views.py - we just needed the on_completion Method to return the response
    def _post(self, request, pk=None, *args, **kwargs):
        chunked_upload = None
        if pk:
            upload_id = pk
        else:
            chunked_upload = self._put_chunk(request, *args,
                                             whole=True, **kwargs)
            upload_id = chunked_upload.id

        checksum = request.data.get(settings.CHECKSUM_TYPE)

        error_msg = None
        if self.do_checksum_check:
            if not upload_id or not checksum:
                error_msg = ("Both 'id' and '{}' are "
                             "required").format(settings.CHECKSUM_TYPE)
        if not upload_id:
            error_msg = "'id' is required"
        if error_msg:
            raise ChunkedUploadError(status=status.HTTP_400_BAD_REQUEST,
                                     detail=error_msg)

        if not chunked_upload:
            chunked_upload = get_object_or_404(self.get_queryset(),
                                               pk=upload_id)

        self.is_valid_chunked_upload(chunked_upload)

        if self.do_checksum_check:
            self.checksum_check(chunked_upload, checksum)

        chunked_upload.completed()

        return self.on_completion(chunked_upload, request)

def does_ept_exist(pointcloud_id):
    return does_processed_model_exist(EPTTempModel.objects.filter(pointcloud_id=pointcloud_id).first())

def does_tif_exist(pointcloud_id):
    return does_processed_model_exist(TIFTempModel.objects.filter(pointcloud_id=pointcloud_id).first())

def does_processed_model_exist(temp_model):
    if temp_model is None:
        return False
    if not temp_model.is_currently_processing:
        temp_model.last_used_date = datetime.now(timezone.utc)
        temp_model.save()
        return True
    try:
        while temp_model.is_currently_processing:
            time.sleep(5)
            temp_model.refresh_from_db()
        return True
    except:
        return False

def get_or_create_temp_path():
    path_string = './frontend/static/temp'
    Path(path_string).mkdir(parents=True, exist_ok=True)
    return path_string

def delete_failed_ept(ept_temp_model):
    if ept_temp_model is not None:
        if os.path.exists(ept_temp_model.save_path):
            shutil.rmtree(ept_temp_model.save_path)
        ept_temp_model.delete()

def delete_failed_tif(tif_temp_model):
    if tif_temp_model is not None:
        if os.path.exists(tif_temp_model.save_path):
            os.remove(tif_temp_model.save_path)
        tif_temp_model.delete()

def get_possible_pointclouds(request):
    if request.user:
        user_owner = User.objects.filter(username=request.user.username)
        return Pointcloud.objects.filter(Q(owner__in=user_owner) | (Q(users_to_share_with__contains=request.user.username) & Q(visibility=Pointcloud.VisibilityMode.SELECTED_GROUP)) | Q(visibility=Pointcloud.VisibilityMode.PUBLIC))
    else:
        return Pointcloud.objects.filter(visibility=Pointcloud.VisibilityMode.PUBLIC)

@api_view(['GET'])
@permission_classes((permissions.AllowAny, ))
def get_pointcloud_3d(request, pointcloud_id):
    """
    GET:
        - Gets the path of the 3D-Pointcloud (EPT) by ID.
    """
    if request.method == 'GET':
        pointcloud = get_possible_pointclouds(request).filter(id=pointcloud_id).first()
        if pointcloud is not None:
            base_path = os.path.basename(pointcloud.pointcloud_filepath)
            ept_folder_path = 'static/temp/'+os.path.splitext(base_path)[0]
            return_path = ept_folder_path+'/ept.json'

            if does_ept_exist(pointcloud_id):
                return Response({"pointcloudpath": return_path}, status=status.HTTP_200_OK)

            ept_temp_model = EPTTempModel.objects.create(pointcloud_id=pointcloud_id, last_used_date=datetime.now(timezone.utc), is_currently_processing=True, save_path=('./frontend/' + ept_folder_path))

            try:
                return_value = pipeline_worker.convert_laz_to_ept_laz(pointcloud.pointcloud_filepath, get_or_create_temp_path(), settings.UNTWINE_BUILD_PATH)
                if return_value != b"":
                    delete_failed_ept(ept_temp_model)
                    return Response({"detail": return_value}, status=status.HTTP_400_BAD_REQUEST)
            except:
                delete_failed_ept(ept_temp_model)
                return Response({"detail": "Processing failed. Please try again later."}, status=status.HTTP_400_BAD_REQUEST)

            ept_temp_model.is_currently_processing = False;
            ept_temp_model.save()
            return Response({"pointcloudpath": return_path}, status=status.HTTP_200_OK)
        return Response({"detail": "Access to the requested pointcloud denied."}, status=status.HTTP_403_FORBIDDEN)

@api_view(['GET'])
@permission_classes((permissions.AllowAny, ))
def get_pointcloud_2d(request, pointcloud_id):
    """
    GET:
        - Gets the path of the 2D-Pointcloud (TIF) by ID.
    """
    if request.method == 'GET':
        pointcloud = get_possible_pointclouds(request).filter(id=pointcloud_id).first()
        if pointcloud is not None:
            base_path = os.path.basename(pointcloud.pointcloud_filepath)
            return_path = 'static/temp/'+os.path.splitext(base_path)[0]+'.tif'

            if does_tif_exist(pointcloud_id):
                return Response({"tifpath": return_path}, status=status.HTTP_200_OK)

            tif_temp_model = TIFTempModel.objects.create(pointcloud_id=pointcloud_id, last_used_date=datetime.now(timezone.utc), is_currently_processing=True, save_path=('./frontend/' + return_path))

            try:
                metadata = pipeline_worker.convert_laz_to_2d(pointcloud.pointcloud_filepath, get_or_create_temp_path())
            except:
                delete_failed_tif(tif_temp_model)
                return Response({"detail": "Processing failed. Please try again later."}, status=status.HTTP_400_BAD_REQUEST)

            tif_temp_model.is_currently_processing = False;
            tif_temp_model.save()
            return Response({"tifpath": return_path}, status=status.HTTP_200_OK)
        return Response({"detail": "Access to the requested pointcloud denied."}, status=status.HTTP_403_FORBIDDEN)

class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.owner.id == request.user.id

class IsAuthorized(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj.owner.id == request.user.id:
            return True
        if obj.visibility == Pointcloud.VisibilityMode.PUBLIC:
            return True
        if request.user in obj.users_to_share_with:
            return True
        return False

def delete_pointcloud(laz_path, pointcloud_id):
    if os.path.exists(laz_path):
        os.remove(laz_path)
    delete_failed_ept(EPTTempModel.objects.filter(pointcloud_id=pointcloud_id).first())
    delete_failed_tif(TIFTempModel.objects.filter(pointcloud_id=pointcloud_id).first())

class PointcloudViewset(viewsets.ModelViewSet):
    """
    list:
    Example - /api/pointclouds?page=2&pageSize=25&search=london
    """
    queryset = Pointcloud.objects.all()
    permission_classes_by_action = {
            'retrieve': [IsOwner],
            'update': [IsOwner],
            'partial_update': [IsOwner],
            'destroy': [IsOwner],
            'default': [permissions.AllowAny]
        }

    #hasattr is needed because of swagger - see comment in accepted answer: https://stackoverflow.com/questions/22616973/django-rest-framework-use-different-serializers-in-the-same-modelviewset
    def get_serializer_class(self):
        if hasattr(self, 'action') and self.action == 'list':
            return PointcloudGetListSerializer
        if hasattr(self, 'action') and self.action == 'retrieve':
            return PointcloudUpdateRetrieveSerializer
        if hasattr(self, 'action') and self.action == 'update':
            return PointcloudUpdateRetrieveSerializer
        if hasattr(self, 'action') and self.action == 'partial_update':
            return PointcloudUpdateRetrieveSerializer
        return PointcloudGetListSerializer # I dont' know what you want for create/destroy/update.

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            return [permission() for permission in self.permission_classes_by_action['default']]

    def get_queryset_filtered_by_parameters(self, qs):
        page = self.request.query_params.get('page')
        page_size = self.request.query_params.get('pageSize')
        search = self.request.query_params.get('search')
        if search is not None:
            qs = qs.filter(
                Q(owner__username__icontains=search) | 
                Q(name__icontains=search) | 
                Q(place__icontains=search) | 
                Q(added_at__icontains=search) |
                Q(scan_date__icontains=search) |
                Q(scan_device__icontains=search) |
                Q(description__icontains=search) |
                Q(visibility__icontains=search) |
                Q(metadata__icontains=search)
            )
        if page is not None and page_size is not None and int(page) > 0:
            page = int(page)
            page_size = int(page_size)
            qs = qs[(page-1)*page_size:page_size*page]
        return qs
    
    def get_queryset(self):
        qs = get_possible_pointclouds(self.request)
        return self.get_queryset_filtered_by_parameters(qs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        delete_pointcloud(instance.pointcloud_filepath, instance.id)
        instance.delete()
        return Response({"detail": "Pointcloud deleted."}, status=status.HTTP_200_OK)