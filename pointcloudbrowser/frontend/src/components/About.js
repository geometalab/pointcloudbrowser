import React from 'react';
import { Header, Icon } from 'semantic-ui-react';
import { useTranslation } from "react-i18next";



function About() {
    const { t } = useTranslation();

    return (
        <div>
            <Header>
                <h2>{t('about.about')}</h2>
            </Header>
            <h3>{t('title')}</h3>
            <p>{t('about.description')}</p>
            <h3>{t('about.howToUse')}</h3>
            <p>{t('about.descriptionUse')}</p>
            <h3>{t('about.license')}</h3>
            <p>{t('about.descriptionLisence')}</p>
            <h3>{t('about.source')}</h3>
            <a href='https://gitlab.ost.ch/nadine.sennhauser/pointcloudbrowser'><Icon fitted name='linkify' />{t('title')}</a>
            <h3>{t('about.resources')}</h3>
            <a href='https://pypi.org/project/pointcloudpipeline'><Icon fitted name='linkify' />{t('about.pointcloudpipeline')}</a>
            <a href='https://github.com/potree/potree'><Icon fitted name='linkify' />{t('about.potree')}</a>
        </div>
    )
}
export default About;