import '@testing-library/jest-dom'
import * as React from 'react'
import { render, screen } from '@testing-library/react'
import About from "./About"
import './../i18n';


describe('Show About', () => {
  test('render About', () => {
    render(<About></About>)
    expect(screen.getByText(/About/i)).toBeInTheDocument()
  })

});