import React, { useState } from 'react';

import {
  Grid,
  Sidebar,
  Button,
  Dimmer,
  Loader,
  Message,
  Popup,
  Icon,
  Modal,
  ModalContent
} from "semantic-ui-react";
import { useTranslation } from "react-i18next";
import List from "./List";
import Login from "./Login"
import Upload from "./Upload"
import { getUser, getView, getPointClouds, getView2d } from "../data/api"
import About from "./About"
import PointcloudNavigator from "./PointcloudNavigator";
import View2d from "./View2d"
import Configuration3d from "./Configuration3d";
import Configuration2d from "./Configuration2d";
import User from "./User"
import jwt_decode from "jwt-decode";

function App() {
  const [visibleList, setVisibleList] = useState(true)
  const [visibleConfiguration3d, setVisibleConfiguration3d] = useState(false)
  const [visibleConfiguration2d, setVisibleConfiguration2d] = useState(false)
  const [visiblePointCloud, setVisiblePointCloud] = useState(true)
  const [visibleLogin, setVisibleLogin] = useState(false)
  const [visibleUpload, setVisibleUpload] = useState(false)
  const [edit, setEdit] = useState(false)
  const [visibleAbout, setVisibleAbout] = useState(false)
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(undefined)
  const [email, setEmail] = useState(undefined)
  const [linkToPointCloud, setLinkToPointCloud] = useState({ paths: undefined, check: 0 })
  const [linkToPointCloud2d, setLinkToPointCloud2d] = useState(undefined)
  const [pointClouds, setPointClouds] = useState([])
  const [page, setpage] = useState(0)
  const [lastPage, setlastPage] = useState(false)
  const [actualPointCloud, setActualPointCloud] = useState(undefined)
  const [errorMessage, setErrorMessage] = useState('')
  const [uploadProgress, setUploadProgress] = useState(0)
  const [open, setOpen] = useState(false)

  const { t } = useTranslation();

  const handleActualPointCloud = (pointCloud) => {
    setLinkToPointCloud({ paths: undefined, check: 0 })
    setActualPointCloud(pointCloud);
    setVisibleConfiguration3d(true);
    setVisiblePointCloud(true);
    setVisibleConfiguration2d(false);
    if (pointCloud) {
      getView(pointCloud.id).then((result) => {
        setLinkToPointCloud({ paths: result, check: 1 });
        handleError('');
      }).catch((error) => {
        handleError(error);
        setLinkToPointCloud({ paths: undefined, check: 1 })
      })
    }
  }

  const handleError = (value) => {
    if (value && typeof value !== 'string') {
      value.response.json().then((response) => {
        setErrorMessage(response.detail);
      })
    } else if (typeof value !== 'string') {
      setErrorMessage(value);
    } else {
      setErrorMessage('');
    }

  }

  const handleUploadProgress = (value) => {
    setUploadProgress(value);
  }

  const handleVisibleUpload = (value) => {
    setVisibleUpload(value)
    setEdit(value);
  }

  const handleVisibleLogin = (value) => {
    setVisibleLogin(value);
  }

  const handleList = (search) => {
    setVisibleList(true);
    return getPointClouds(search, 1).then((response) => {
      setlastPage(false)
      setPointClouds(response);
      handleActualPointCloud(response[0]);
      setpage(1);
      handleError("");
      if (response.length < 25) {
        setlastPage(true)
      }
    }).catch((error) => {
      setpage(0);
      handleError(error);
    })
  }

  const appendList = (search) => {
    return getPointClouds(search, page + 1)
      .then((response) => {
        setPointClouds([...pointClouds, ...response]);
        setpage(page + 1);
        handleError("");
        if (response.length < 25) {
          setlastPage(true)
        }
      }).catch((error) => {
        handleError(error);
      })
  }

  const handleLogout = () => {
    localStorage.removeItem('token');
    setUser(undefined);
    setEmail(undefined);
    handleList("");
  }

  const handle2D = () => {
    setLinkToPointCloud2d(undefined);
    setVisibleConfiguration2d(!visibleConfiguration2d);
    setVisiblePointCloud(false);
    setVisibleConfiguration3d(false);
    if (actualPointCloud) {
      getView2d(actualPointCloud.id).then((result) => {
        setLinkToPointCloud2d(result);
        handleError('');
      }).catch((error) => {
        handleError(error)
      })
    }
  }

  if (loading) {
    return (
      <Dimmer active inverted>
        <Loader inverted>{t('loading')}</Loader>
      </Dimmer>

    )
  }

  if (localStorage.getItem('token') && !user) {
    if (jwt_decode(localStorage.getItem('token')).exp < (Date.now()) / 1000) {
      handleLogout();
    } else {
      setLoading(true);
      getUser().then((response) => {
        setUser(response.username);
        setEmail(response.email);
        setLoading(false);
      }).catch((error) => {
        setLoading(false);
        handleLogout();
      });
    }
  }

  if (pointClouds.length == 0 && page == 0 && (errorMessage == '' || !errorMessage)) {
    setLoading(true);
    handleList("").then(() => {
      setLoading(false);
      setpage(1);
    }).catch((error) => {
      setLoading(false);
    })
  }
  return (
    <div>
      <Button
        className={(visibleConfiguration2d || visibleConfiguration3d ? "twoDButton openRight" : "twoDButton") + (!visiblePointCloud ? " actual" : "")}
        onClick={() => handle2D()}
      >
        <Icon name={visibleConfiguration2d ? 'triangle right' : 'triangle left'} />
        {t('2dConfiguration')}
      </Button>
      <Button
        className={visibleList ? "listButton openLeft" : "listButton"}
        onClick={() => setVisibleList(!visibleList)}
      >
        {visibleList ? '' : t('listButton')}
        <Icon name={visibleList ? 'triangle left' : 'triangle right'} />
      </Button>
      <Button
        className={(visibleConfiguration3d || visibleConfiguration2d ? "treeDButton openRight" : "treeDButton") + (visiblePointCloud ? " actual" : "")}
        onClick={() => {
          setVisibleConfiguration3d(!visibleConfiguration3d);
          setVisiblePointCloud(true);
          setVisibleConfiguration2d(false);
        }}
      >
        <Icon name={visibleConfiguration3d ? 'triangle right' : 'triangle left'} />
        {t('3dConfiguration')}
      </Button>

      <Grid columns={1} stretched className='position'>
        <Grid.Row stretched>
          <Grid.Column stretched>
            <div className="flex spaceBetween alignCenter">
              <div className="flex">
                <h1>{t('title')}</h1>
                <Modal
                  closeIcon
                  onClose={() => setVisibleAbout(false)}
                  onOpen={() => setVisibleAbout(true)}
                  open={visibleAbout}
                  trigger={<Button inverted color='green'>{t('about.about')}</Button>}>
                  <ModalContent><About /></ModalContent>
                </Modal>

              </div>
              {user ?
                <div>
                  <Modal
                    closeIcon
                    onClose={() => {
                      setVisibleUpload(false);
                      setEdit(false)
                    }}
                    onOpen={() => setVisibleUpload(true)}
                    open={visibleUpload}
                    trigger={<Button loading={uploadProgress !== 0}>{t('uploadButton')}</Button>}>
                    <ModalContent scrolling>
                      <Upload pointCloud={edit ? actualPointCloud : undefined}
                        handleUploadError={handleError}
                        handleUploadProgress={handleUploadProgress}
                        handleVisibleUpload={handleVisibleUpload}
                        handleList={handleList} />
                    </ModalContent>
                  </Modal>
                  <Modal
                    closeIcon
                    onClose={() => {
                      setOpen(false);
                      setEdit(false);
                    }
                    }
                    onOpen={() => setOpen(true)}
                    open={open}
                    trigger={<Button>{user}</Button>}>
                    <ModalContent><User email={email} user={user} /></ModalContent>
                    <Modal.Actions>
                      <Button
                        content={t('logout')}
                        onClick={() => {
                          setOpen(false);
                          if (window.confirm(t('confirm'))) handleLogout();
                        }}
                      />
                    </Modal.Actions>
                  </Modal>
                </div>
                :
                <div>
                  <Popup content={t('info')} trigger={
                    <Button>{t('uploadButton')}</Button>
                  } />
                  <Modal
                    closeIcon
                    onClose={() => setVisibleLogin(false)}
                    onOpen={() => setVisibleLogin(true)}
                    open={visibleLogin}
                    trigger={<Button>{t('login.login')}</Button>}>
                    <ModalContent>
                      <Login handleVisibleLogin={handleVisibleLogin} handleList={handleList} />
                    </ModalContent>
                  </Modal>
                </div>
              }
            </div>
            {(uploadProgress !== 0) &&
              <Message icon>
                <Icon name='circle notched' loading />
                <Message.Content>
                  <Message.Header>{t('uploadMessageHeader')}</Message.Header>
                  {(uploadProgress !== 100) ? t('uploadMessage', { uploadProgress: uploadProgress }) : t('processMessage')}
                </Message.Content>
              </Message>
            }
            {((visiblePointCloud && linkToPointCloud.check == 0) ||
              (!visiblePointCloud && !linkToPointCloud2d)) && actualPointCloud && (errorMessage == '' || !errorMessage) &&
              <Message icon>
                <Icon name='circle notched' loading />
                <Message.Content>
                  <Message.Header>{t('viewMessageHeader')}</Message.Header>
                  {t('viewProcessMessage')}
                </Message.Content>
              </Message>}
            {(errorMessage !== '' && errorMessage != undefined) && <Message error content={errorMessage} />}
          </Grid.Column>
          <Grid.Column stretched>
            <Sidebar.Pushable raised>
              <Sidebar
                className='SidebarLeft'
                animation='overlay'
                direction='left'
                icon='labeled'
                vertical
                visible={visibleList}
              >
                <List
                  handleList={handleList}
                  pointClouds={pointClouds}
                  lastPage={lastPage}
                  actualPointcloud={actualPointCloud}
                  handleActualPointCloud={handleActualPointCloud}
                  appendList={appendList} />
              </Sidebar>
              <div className={visiblePointCloud ? "pointcloud" : "tifView"}>
                {actualPointCloud && visiblePointCloud && linkToPointCloud.paths && <PointcloudNavigator actualPointcloud={linkToPointCloud.paths} />}
                {actualPointCloud && !visiblePointCloud && linkToPointCloud2d && <View2d actualPointcloud={linkToPointCloud2d} handleError={handleError} />}
              </div>
              <Sidebar
                className='SidebarRight'
                animation='overlay'
                direction='right'
                vertical
                visible={visibleConfiguration3d}
              >
                <Configuration3d pointCloud={actualPointCloud} user={user} handleVisibleUpload={handleVisibleUpload} />
              </Sidebar>

              <Sidebar
                className='SidebarRight'
                animation='overlay'
                direction='right'
                vertical
                visible={visibleConfiguration2d}
              >
                <Configuration2d pointCloud={actualPointCloud} url={linkToPointCloud2d} />
              </Sidebar>

            </Sidebar.Pushable>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>

  )
}


export default App;
