import '@testing-library/jest-dom'
import * as React from 'react'
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react'
import App from "./App"
import './../i18n';

jest.mock('./Configuration3d.js', () => () => {
  return <p>Configuration3d</p>;
});

jest.mock('./Configuration2d.js', () => () => {
  return <p>Configuration2d</p>;
});

jest.mock('./PointcloudNavigator.js', () => () => {
  return <p>PointcloudNavigator</p>;
});
jest.mock('./List.js', () => () => {
  return <p>Listentries</p>;
});

jest.mock("../data/api.js", () => {
  const pointClouds = [{ "name": "test", "place": "Rapperswil", "added_at": "2021-05-26T08:00:23.905262Z", "scan_date": "2021-03-23", "scan_device": "IPad pro", "description": "hjsfsdf jhnbfhkfb jhfearkb ekgrhbe\nkjfjknf djfnfdvfdjsfd nfvfjfkhf   ,dsfneb sfjnewfipwjf wekbfreruibgf sfberubtf sf eurib sfvfiwbfuie84", "visibility": "private", "metadata": 1 }, { "name": "tet", "place": "", "added_at": "2021-05-26T11:12:30.021090Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "group", "metadata": 1 }, { "name": "tet", "place": "", "added_at": "2021-05-26T11:16:26.005059Z", "scan_date": "2021-05-04", "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "tre", "place": "", "added_at": "2021-05-26T11:20:36.007427Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "tat", "place": "", "added_at": "2021-05-26T11:24:08.611420Z", "scan_date": "2021-04-28", "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "test", "place": "", "added_at": "2021-05-26T11:26:41.347016Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:31:42.524110Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:31:59.363802Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:36:31.860508Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:37:09.203797Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:39:47.706339Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:46:36.463082Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:49:59.905307Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }, { "name": "t", "place": "", "added_at": "2021-05-26T11:53:33.290115Z", "scan_date": null, "scan_device": "", "description": "", "visibility": "public", "metadata": 1 }];
  return {
    getPointClouds: jest.fn(() => Promise.resolve(pointClouds))
  };
});


describe('Show landingpage', () => {
  test('render App', () => {
    act(() => {
      render(<App></App>)
    });
    expect(screen.queryByText(/Loading/i)).toBeInTheDocument()
  })

});

