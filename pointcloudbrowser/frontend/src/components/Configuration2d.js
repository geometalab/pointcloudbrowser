import React from 'react';
import Description from './Description';
import { useTranslation } from "react-i18next";

function Configuration2d(props) {
    const { t } = useTranslation();
    if (props.pointCloud) {
        return (
            <div>
                <Description pointCloud={props.pointCloud} url={props.url ? props.url.tifpath : undefined} />
                <h3 className='min'>{t('configuration2d.informations')}</h3>
                <div className="margin">
                    <p>{t('configuration2d.calculate')}</p>
                    <p>{t('configuration2d.usage')}</p>
                </div>
            </div>
        )
    }
    return (
        <div className="margin">
            <p>{t('list.placeholder')}</p>
        </div>
    )
}
export default Configuration2d;
