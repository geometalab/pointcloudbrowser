import React from 'react';
import { useTranslation } from "react-i18next";
import Description from './Description';

function Configuration3d(props) {
    const { t } = useTranslation();
    const handleVisibleUpload = (value) => {
        props.handleVisibleUpload(value)
    }
    if (props.pointCloud) {
        return (
            <div>
                <Description
                    pointCloud={props.pointCloud}
                    url={props.pointCloud.pointcloud_download_filepath}
                    user={props.user}
                    handleVisibleUpload={handleVisibleUpload} />
                <div id="potree_sidebar_container"></div>
            </div>
        )
    }
    return (
        <div className="margin">
            <p>{t('list.placeholder')}</p>
        </div>
    )
}
export default Configuration3d;