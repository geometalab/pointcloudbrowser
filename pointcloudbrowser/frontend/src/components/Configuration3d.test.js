import * as React from 'react'
import { render, screen } from '@testing-library/react'
import Configuration3d from './Configuration3d';
import './../i18n';
import '@testing-library/jest-dom'


describe('Show Configuration 3d', () => {
  test('no point cloud', () => {
    render(<Configuration3d pointCloud={undefined} />)
    expect(screen.getByText('No point clouds found')).toBeInTheDocument()
  })
  test('with point cloud', () => {
    const pointCloud = {
      added_at: "2021-06-04T11:26:35.425254Z",
      description: "",
      id: 6,
      metadata: "{test}",
      name: "test3",
      owner_name: "test",
      place: "",
      scan_date: "2021-05-18",
      scan_device: "",
      users_to_share_with: [],
      visibility: "public"
    }
    render(<Configuration3d pointCloud={pointCloud} />)
    expect(screen.getByText('test3')).toBeInTheDocument()
  })

});