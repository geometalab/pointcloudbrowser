import '@testing-library/jest-dom'
import * as React from 'react'
import { render, screen } from '@testing-library/react'
import Configuration2d from './Configuration2d';
import './../i18n';


describe('Show Configuration 2d', () => {
  test('no point cloud', () => {
    render(<Configuration2d pointCloud={undefined} />)
    expect(screen.getByText('No point clouds found')).toBeInTheDocument()
  })
  test('with point cloud', () => {
    const pointCloud = { "name": "test1", "place": "test2", "added_at": "2021-05-26T11:39:47.706339Z", "scan_date": "2021-05-12", "scan_device": "Leica BLK2GO", "description": "Test3", "visibility": "public", "metadata": 1 };
    render(<Configuration2d pointCloud={pointCloud} />)
    expect(screen.getByText('test1')).toBeInTheDocument()
  })

});