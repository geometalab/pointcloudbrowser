import React from 'react';
import { useTranslation } from "react-i18next";
import { Button, Icon } from 'semantic-ui-react';

function Description(props) {
    const { t } = useTranslation();
    return (
        <div>
            <h2>{props.pointCloud.name}</h2>
            <div className='margin'>
                <p>{t('listEntry.description') + props.pointCloud.description}</p>
                <p>{t('listEntry.place') + props.pointCloud.place}</p>
                <p>{t('listEntry.captureDate') + (props.pointCloud.scan_date != null ? props.pointCloud.scan_date : '')}</p>
                <p>{t('listEntry.uploadUser') + props.pointCloud.owner_name}</p>
                <p>{t('listEntry.release') + props.pointCloud.visibility}</p>
                <p>{t('listEntry.scannDevice') + props.pointCloud.scan_device}</p>
                <p>{t('listEntry.uploadDate') + props.pointCloud.added_at.toString().split("T")[0]}</p>
                <div className={props.user == props.pointCloud.owner_name ? 'around' : ''}>
                    {props.url ? <a href={props.url} download><Icon disabled name='download' />{t('description.download')}</a> :
                        <Button loading>{t('description.download')}</Button>}
                    {props.user == props.pointCloud.owner_name && <Button onClick={() => { props.handleVisibleUpload(true) }}>{t('description.edit')}</Button>}
                </div>
            </div>
        </div>
    )
}
export default Description;