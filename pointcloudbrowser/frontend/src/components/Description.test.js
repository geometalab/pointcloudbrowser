import '@testing-library/jest-dom'
import * as React from 'react'
import { render, screen } from '@testing-library/react'
import Description from './Description';
import './../i18n';


describe('Show Description', () => {
    const pointCloud = {
        added_at: "2021-06-04T11:26:35.425254Z",
        description: "",
        id: 6,
        metadata: "{test}",
        name: "test3",
        owner_name: "test",
        place: "",
        scan_date: "2021-05-18",
        scan_device: "",
        users_to_share_with: [],
        visibility: "public"
    }
    test('only with point cloud', () => {
        render(<Description pointCloud={pointCloud} />)
        expect(screen.getByText('test3')).toBeInTheDocument()
        expect(screen.queryByText(/Edit/i)).toBeNull()
    })
    test('with point cloud and url', () => {
        render(<Description pointCloud={pointCloud} url="test/test.ch" />)
        expect(screen.getByText('Download')).toBeInTheDocument()
        expect(screen.queryByText(/Edit/i)).toBeNull()
    })
    test('with point cloud, url and maching user', () => {
        render(<Description pointCloud={pointCloud} url="test/test.ch" user="test" />)
        expect(screen.getByText('Download')).toBeInTheDocument()
        expect(screen.getByText('Edit')).toBeInTheDocument()
    })
    test('with point cloud, url and not maching user', () => {
        render(<Description pointCloud={pointCloud} url="test/test.ch" user="hans" />)
        expect(screen.getByText('Download')).toBeInTheDocument()
        expect(screen.queryByText(/Edit/i)).toBeNull()
    })
});