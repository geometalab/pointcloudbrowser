import React, { useState } from 'react';
import { Input, Form, Button, Icon } from 'semantic-ui-react';
import { useTranslation } from "react-i18next";
import ListEntry from "./ListEntry";


function List(props) {
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation();

  const handleChange = (value) => {
    props.handleActualPointCloud(value);
  }

  const onSubmit = event => {
    event.preventDefault();
    setLoading(true);
    props.handleList(search)
      .then(setLoading(false))
      .catch(setLoading(false))
  };

  const reload = () => {
    setLoading(true);
    props.appendList(search)
      .then((response) => {
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }
  return (
    <div className="pointcloudsList">
      <h2>{t('list.title')}</h2>
      <Form>
        <Input
          onChange={(event) => setSearch(event.target.value)}
          icon="search"
          iconPosition="left"
          placeholder={t('list.search')}
          value={search}
        />
        <Button
          primary
          loading={loading}
          onClick={onSubmit}
          icon>
          <Icon name="search" />
        </Button>
      </Form>
      <div className="scrollList">
        {props.actualPointcloud ?
          props.pointClouds.map((pointCloud) => (
            <ListEntry actualPointcloud={props.actualPointcloud} pointCloud={pointCloud} key={pointCloud.id} handleChange={handleChange} />
          )) : <p>{t('list.placeholder')}</p>}
        {!props.lastPage && <Button className='load' onClick={() => reload()} loading={loading}>{t('list.reload')}</Button>}
      </div>
    </div>

  )
}
export default List;