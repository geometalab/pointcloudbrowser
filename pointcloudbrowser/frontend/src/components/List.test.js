import '@testing-library/jest-dom'
import * as React from 'react'
import { render, screen } from '@testing-library/react'
import List from './List';
import './../i18n';


describe('Show List', () => {

  test('no point clouds', () => {
    render(<List pointClouds={[]} lastPage={true} />)
    expect(screen.getByText('No point clouds found')).toBeInTheDocument()
    expect(screen.queryByText(/Load more/i)).toBeNull()
  })
  test('last page', () => {
    const pointClouds = [{
      added_at: "2021-06-04T11:26:35.425254Z",
      description: "",
      id: 6,
      metadata: "{test}",
      name: "test3",
      owner_name: "test",
      place: "",
      scan_date: "2021-05-18",
      scan_device: "",
      users_to_share_with: [],
      visibility: "public"
    },
    {
      added_at: "2021-06-04T11:26:35.425254Z",
      description: "test",
      id: 5,
      metadata: "{test}",
      name: "test3",
      owner_name: "testIT",
      place: "",
      scan_date: "2021-05-18",
      scan_device: "",
      users_to_share_with: [],
      visibility: "public"
    }]
    render(<List pointClouds={pointClouds} lastPage={true} actualPointcloud={pointClouds[1]} />)
    expect(screen.queryByText(/Load more/i)).toBeNull()
  })
  test('with point cloud not lastpage', () => {
    const pointClouds = [{
      added_at: "2021-06-04T11:26:35.425254Z",
      description: "",
      id: 6,
      metadata: "{test}",
      name: "test3",
      owner_name: "test",
      place: "",
      scan_date: "2021-05-18",
      scan_device: "",
      users_to_share_with: [],
      visibility: "public"
    },
    {
      added_at: "2021-06-04T11:26:35.425254Z",
      description: "test",
      id: 5,
      metadata: "{test}",
      name: "test3",
      owner_name: "testIT",
      place: "",
      scan_date: "2021-05-18",
      scan_device: "",
      users_to_share_with: [],
      visibility: "public"
    }]
    render(<List pointClouds={pointClouds} lastPage={false} actualPointcloud={pointClouds[1]} />)
    expect(screen.getByText('Load more')).toBeInTheDocument()
  })

});
