import React from 'react';
import { useTranslation } from "react-i18next";
import { Button } from 'semantic-ui-react';

function ListEntry(props) {
    const { t } = useTranslation();
    return (
        <Button className={(props.actualPointcloud && props.actualPointcloud.id == props.pointCloud.id) ? "listEntry select" : "listEntry"} onClick={() => props.handleChange(props.pointCloud)}>
            <h3>{props.pointCloud.name}</h3>
            <p>{t('listEntry.uploadDate') + props.pointCloud.added_at.toString().split("T")[0]}</p>
            <p>{t('listEntry.uploadUser') + props.pointCloud.owner_name}</p>
            <p>{t('listEntry.place') + props.pointCloud.place}</p>
            <p className="state">{props.pointCloud.visibility}</p>
            <span className={props.pointCloud.visibility}></span>
        </Button>
    )
}
export default ListEntry;