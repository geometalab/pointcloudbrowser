import React, { useState } from 'react';
import { Input, Segment, Form, Button, Message, } from 'semantic-ui-react';
import { useTranslation } from "react-i18next";
import { authenticate } from "../data/api"
import Register from "./Register"

function Login(props) {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [loginError, setLoginError] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const [register, setRegister] = useState(false);

  const { t } = useTranslation();

  const handleVisibleRegister = () => {
    props.handleVisibleLogin(false);
    handleList("");
  }

  const handleRegister = () => {
    setRegister(false);
  }

  const onSubmit = event => {
    event.preventDefault();
    setLoading(true);
    setLoginError(undefined);
    authenticate(login.trim(), password)
      .then((token) => {
        localStorage.setItem('token', token.token);
        setLoading(false);
        props.handleVisibleLogin(false);
        props.handleList("");
      })
      .catch((error) => {
        setLoading(false);
        setLoginError(error);
      });
  };

  if (register) {
    return <Register handleRegister={handleRegister} handleVisibleRegister={handleVisibleRegister}/>
  }

  return (
    <Form error={loginError}>
      <Segment secondary>
        <h2>{t('login.title')}</h2>
        <Form.Field>
          <label>{t('login.username')}
            <Input
              onChange={(event) => setLogin(event.target.value)}
              icon="user"
              iconPosition="left"
              placeholder={t('login.username')}
              value={login}
            />
          </label>
        </Form.Field>
        <Form.Field>
          <label>{t('login.password')}
            <Input
              onChange={(event) => setPassword(event.target.value)}
              icon="lock"
              iconPosition="left"
              placeholder={t('login.password')}
              type="password"
              value={password}
            />
          </label>
        </Form.Field>
        <Button
          primary
          loading={loading}
          onClick={onSubmit}
          fluid={true}
          content={t('login.send')}
          disabled={login.trim().length == 0 || password.length == 0}
        />
      </Segment>
      {loginError ? <Message error content={t('login.error2')} /> : null}
      <Button secondary onClick={() => setRegister(true)} size="large">
        {t('login.register')}
      </Button>
    </Form>
  )

}
export default Login;