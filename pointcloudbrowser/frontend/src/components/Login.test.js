import '@testing-library/jest-dom'
import * as React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import Login from "./Login"
import './../i18n';

describe('Show Login', () => {

    test('render App Login', () => {
        render(<Login></Login>)
        expect(screen.getByText(/Log-in/i)).toBeInTheDocument()
    })

    test('activate Login', () => {
        render(<Login></Login>)
        expect(screen.getByText(/Log-in/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Password/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Log-in/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Username/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Log-in/i)).not.toBeDisabled();
        fireEvent.change(screen.getByLabelText(/Password/i), {
            target: { value: '' },
        })
        expect(screen.getByText(/Log-in/i)).toHaveAttribute('disabled');
    })

    test('change to register', () => {
        render(<Login></Login>)
        expect(screen.queryByText(/register/i)).toBeNull()
        fireEvent.click(screen.getByText(/no account yet/i))
        expect(screen.getByText(/register/i)).toBeInTheDocument()
    })

});