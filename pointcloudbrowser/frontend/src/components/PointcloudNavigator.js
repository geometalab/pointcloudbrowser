import React from 'react';

const Potree = window.Potree
const THREE = window.THREE
const TWEEN = window.TWEEN

export default class PointcloudNavigator extends React.Component {
    constructor(props) {
        super(props)
        this.potreeContainerDiv = React.createRef();
    }

    componentDidMount() {
        const viewerElem = this.potreeContainerDiv.current
        window.viewer = new Potree.Viewer(viewerElem);

        window.viewer.setEDLEnabled(true);
        window.viewer.setFOV(60);
        window.viewer.setPointBudget(1000 * 1000);
        window.viewer.setClipTask(Potree.ClipTask.SHOW_INSIDE);
        window.viewer.loadSettingsFromURL();

        window.viewer.setControls(window.viewer.earthControls)

        let url = this.props.actualPointcloud.pointcloudpath
        Potree.loadPointCloud(url).then(e => {
            let pointcloud = e.pointcloud;
            let material = pointcloud.material;

            material.activeAttributeName = "rgba";
            material.minSize = 2;
            material.pointSizeType = Potree.PointSizeType.FIXED

            window.viewer.scene.addPointCloud(pointcloud);

            window.viewer.fitToScreen();
        }, e => console.err("ERROR: ", e));

        window.viewer.loadGUI();
    }

    render() {
        return (
            <div id="potree-root">
                <div className="potree_container">
                    <div id="potree_render_area" ref={this.potreeContainerDiv}></div>

                </div>
            </div>
        )
    }
}