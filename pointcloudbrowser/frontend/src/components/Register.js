import React, { useState } from 'react';
import { Input, Segment, Form, Button, Message } from 'semantic-ui-react';
import { useTranslation } from "react-i18next";
import { signup } from "../data/api"
import Login from './Login';

function Register(props) {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [passwordCheck, setpasswordCheck] = useState("");
  const [registerError, setRegisterError] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation();

  const onSubmit = event => {
    event.preventDefault();
    setLoading(true);
    signup(login.trim(), password, email.trim())
      .then((token) => {
        localStorage.setItem('token', token.token);
        setLoading(false);
        setRegisterError(undefined);
        props.handleVisibleRegister();
      })
      .catch((error) => {
        setLoading(false);
        setRegisterError(error);
      });
  };

  return (
    <Form error={registerError}>
      <Segment secondary>
        <h2>{t('login.register')}</h2>
        <Form.Field>
          <label>{t('login.username')}
            <Input
              onChange={(event) => setLogin(event.target.value)}
              icon="user"
              iconPosition="left"
              placeholder={t('login.username')}
              value={login}
              required
              error={login.trim().length == 0 || login.trim().includes(" ")}
            /></label>
        </Form.Field>
        {login.trim().includes(" ") && <p className="inputError">{t('login.usernameError')}</p>}
        <Form.Field>
          <label>{t('login.email')}
            <Input
              onChange={(event) => { setEmail(event.target.value); }}
              icon="mail"
              iconPosition="left"
              placeholder={t('login.email')}
              value={email}
              type="email"
              error={
                (email.length != 0) && (email.trim().includes(" ") || email.includes(" ") || (email.indexOf("@") <= 1 ||
                  email.lastIndexOf(".") < email.indexOf("@") + 2 ||
                  email.lastIndexOf(".") + 2 > email.length))}
            />
          </label>
        </Form.Field>
        {(!email.length == 0) && (email.trim().includes(" ") || (email.indexOf("@") <= 1 || email.lastIndexOf(".") < email.indexOf("@") + 2 || email.lastIndexOf(".") + 2 > email.length)) &&
          <p className="inputError">{t('login.emailError')}</p>}
        <Form.Field>
          <label>{t('login.password')}
            <Input
              onChange={(event) => setPassword(event.target.value)}
              icon="lock"
              iconPosition="left"
              placeholder={t('login.password')}
              type="password"
              value={password}
              error={password.length < 12 || password == passwordCheck}
            />
          </label>

        </Form.Field>
        {(password.length < 12) && <p className="inputError">{t('login.passwordError')}</p>}
        <Form.Field>
          <label>{t('login.repeatPassword')}
            <Input
              onChange={(event) => setpasswordCheck(event.target.value)}
              icon="lock"
              iconPosition="left"
              placeholder={t('login.repeatPassword')}
              type="password"
              value={passwordCheck}
              error={password == passwordCheck}
            />
          </label>
        </Form.Field>
        {(password == passwordCheck) ? null : <p className="inputError">{t('login.repeatPasswordError')}</p>}
        <Button
          primary
          loading={loading}
          onClick={onSubmit}
          fluid={true}
          content={t('login.save')}
          disabled={
            login.trim().includes(" ") ||
            password != passwordCheck ||
            password.length < 12 ||
            login.trim().length == 0 ||
            email.trim().length == 0 ||
            ((!email.length == 0) && (email.trim().includes(" ") || email.indexOf("@") <= 1 ||
              email.lastIndexOf(".") < email.indexOf("@") + 2 ||
              email.lastIndexOf(".") + 2 > email.length))}
        />
      </Segment>
      {registerError ? <Message error content={t('login.error')} /> : null}
      <Button secondary onClick={() => props.handleRegister()} size="large">
        {t('login.back')}
      </Button>
    </Form>
  )

}
export default Register;