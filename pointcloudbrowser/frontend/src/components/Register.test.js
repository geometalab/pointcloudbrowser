import '@testing-library/jest-dom'
import * as React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import Register from "./Register"
import './../i18n';

describe('Show Register', () => {

    test('render App Register', () => {
        render(<Register></Register>)
        expect(screen.getByText(/register/i)).toBeInTheDocument()
    })

    test('activate Register', () => {
        render(<Register></Register>)
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Username/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Email/i), {
            target: { value: 'test@ost.ch' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText('Password'), {
            target: { value: 'test1234test1234' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Password repeat/i), {
            target: { value: 'test1234test1234' },
        })
        expect(screen.getByText(/Register/i)).not.toBeDisabled();
    })

    test('false username', () => {
        render(<Register></Register>)
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Username/i), {
            target: { value: 'te st' },
        })
        expect(screen.getByText(/space/i)).toBeInTheDocument()
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Email/i), {
            target: { value: 'test@ost.ch' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText('Password'), {
            target: { value: 'test1234test1234' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Password repeat/i), {
            target: { value: 'test1234test1234' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
    })

    test('short password', () => {
        render(<Register></Register>)
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Username/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Email/i), {
            target: { value: 'test@ost.ch' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText('Password'), {
            target: { value: 'test1234' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Password repeat/i), {
            target: { value: 'test1234' },
        })
        expect(screen.getByText(/12/i)).toBeInTheDocument()
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
    })

    test('passwords do not match', () => {
        render(<Register></Register>)
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Username/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Email/i), {
            target: { value: 'test@ost.ch' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText('Password'), {
            target: { value: 'test1234test123' },
        })
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Password repeat/i), {
            target: { value: 'test1234test12' },
        })
        expect(screen.getByText(/same/i)).toBeInTheDocument()
        expect(screen.getByText(/Register/i)).toHaveAttribute('disabled');
    })

});