import React, { useState } from 'react';
import { Radio, Input, TextArea, Segment, Form, Button, FormField, Dropdown, Icon } from 'semantic-ui-react';
import { useTranslation } from "react-i18next";
import { sendPointCloud, editMetadata, deletePointCloud } from "../data/api"

function Upload(props) {
  const [name, setName] = useState(props.pointCloud ? props.pointCloud.name : "");
  const [description, setDescription] = useState(props.pointCloud ? props.pointCloud.description : "");
  const [place, setPlace] = useState(props.pointCloud ? props.pointCloud.place : "");
  const [scannDate, setScannDate] = useState(props.pointCloud ? props.pointCloud.scan_date : "");
  const [device, setDevice] = useState(props.pointCloud ? props.pointCloud.scan_device : "");
  const [releaseType, setReleasedType] = useState(props.pointCloud ? props.pointCloud.visibility : 'public');
  const [releasedUsers, setReleasedUsers] = useState(props.pointCloud ? props.pointCloud.users_to_share_with : []);
  const [pointCloud, setPointCloud] = useState();
  const [loading, setLoading] = useState(false);

  const handleChange = (event, { value }) => setReleasedType(value);
  const handleRemove = (user) => {
    setReleasedUsers(releasedUsers.filter(function (element) { return element != user; }));
  };
  const callbackProgress = (value) => {
    props.handleUploadProgress(value)
  }
  const callbackError = (error) => {
    props.handleUploadError(error);
    props.handleUploadProgress(0);
  }
  const callbackReloadList = () => {
    props.handleList("");
  }
  const handleUploadFile = (event) => {
    props.handleUploadError("");
    event.preventDefault();
    props.handleVisibleUpload(false);
    callbackProgress(1);
    const metadata = { name: name.trim(), description: description.trim(), place: place.trim(), scan_date: scannDate, scan_device: device.trim(), visibility: releaseType, users_to_share_with: releaseType=='group' ? releasedUsers: [] }
    sendPointCloud(metadata, pointCloud, props.pointCloud, callbackProgress, callbackError, callbackReloadList);
  }

  const { t } = useTranslation();

  const onSubmit = event => {
    handleUploadFile(event);
  };
  const cleanUp = () => {
    props.handleVisibleUpload(false);
    setLoading(false);
    callbackReloadList();
  }
  const handleError = error =>  {
    props.handleUploadError(error);
    props.handleVisibleUpload(false);
    setLoading(false);
  }

  const onEdit = event => {
    if (pointCloud) {
      handleUploadFile(event);
    } else {
      props.handleUploadError("");
      event.preventDefault();
      setLoading(true);
      const metadata = { name: name.trim(), description: description.trim(), place: place.trim(), scan_date: scannDate, scan_device: device.trim(), visibility: releaseType, users_to_share_with: releaseType=='group' ? releasedUsers: [] }
      editMetadata(metadata, props.pointCloud.id)
        .then((result) => {
          cleanUp();
        }
        ).catch((error) => {
          handleError(error)
        });
    }
  };

  const onDelete = event => {
    props.handleUploadError("");
    event.preventDefault();
    setLoading(true);
    deletePointCloud(props.pointCloud.id).then((result) => {
      cleanUp();
    }).catch((error) => {
      handleError(error)
    });
  };

  return (
    <Form>
      <Segment secondary>
        <h2>{props.pointCloud ? t('upload.edit') : t('upload.title')}</h2>
        <Form.Field>
          <label>{t('upload.name')}
            <Input
              onChange={(event) => setName(event.target.value)}
              placeholder={t('upload.name')}
              value={name}
              required
              error={name.trim().length == 0}
            /></label>
        </Form.Field>
        <Form.Field>
          <label>{t('upload.scannDate')}
            <Input
              onChange={(event) => setScannDate(event.target.value)}
              placeholder={t('upload.scannDate')}
              value={scannDate}
              type="date"
            />
          </label>
        </Form.Field>
        <Form.Field>
          <label>{t('upload.description')}
            <TextArea
              onChange={(event) => { setDescription(event.target.value); }}
              placeholder={t('upload.description')}
              value={description}
            />
          </label>
        </Form.Field>
        <Form.Field>
          <label>{t('upload.place')}
            <Input
              onChange={(event) => setPlace(event.target.value)}
              placeholder={t('upload.place')}
              value={place}
            />
          </label>
        </Form.Field>
        <Form.Field>
          <label>{t('upload.device')}
            <Input
              onChange={(event) => setDevice(event.target.value)}
              placeholder={t('upload.device')}
              value={device}
            />
          </label>
        </Form.Field>
        {props.pointCloud &&
          <Form.Field>
            <a href={props.pointCloud.pointcloud_download_filepath} download><Icon disabled name='download' />{t('upload.downloadOld')}</a>
          </Form.Field>}
        <Form.Field>
          <label>{t('upload.file')}
            <input
              type="file"
              accept=".laz, .LAZ, .las, .e57, .ply"
              onChange={(event) => setPointCloud(event.target.files[0])}
              required={!props.pointCloud}
            />
          </label>
          {(props.pointCloud && pointCloud) && <p className="error">{t('upload.replaceOld')}</p>}
        </Form.Field>
        <Form.Field>
          <label>{t('upload.releaseType')}
            <Form.Field>
              <Radio
                label={t('upload.private')}
                name='radioGroup'
                value='private'
                checked={releaseType === 'private'}
                onChange={handleChange}
              />
            </Form.Field>
            <Form.Field>
              <Radio
                label={t('upload.public')}
                name='radioGroup'
                value='public'
                checked={releaseType === 'public'}
                onChange={handleChange}
              />
            </Form.Field>
            <FormField>
              <Radio
                label={t('upload.group')}
                name='radioGroup'
                value='group'
                checked={releaseType === 'group'}
                onChange={handleChange}
              />
            </FormField>
          </label>
        </Form.Field>
        {releaseType == 'group' &&
          <Form.Field>
            <label>{t('upload.users')}
              <Dropdown
                error={!releasedUsers}
                search
                selection
                multiple
                fluid={true}
                allowAdditions
                value={releasedUsers}
                onChange={
                  (event, { value }) => {
                    setReleasedUsers(value)
                  }
                }
              />
            </label>
          </Form.Field>
        }
        {(releaseType == 'group' && releasedUsers.length >= 1) && releasedUsers.map((user) => (
          <Button content={user} key={user} onClick={() => handleRemove(user)} icon="remove circle" />
        ))}
        {props.pointCloud ?
          <div>
            <Button
              loading={loading}
              className="send"
              primary
              onClick={(event) => onEdit(event)}
              fluid={true}
              content={t('upload.editSave')}
              disabled={name.length == 0 || (releaseType == 'group' && releasedUsers.length == 0)}
            />
            <Button
              loading={loading}
              className="send"
              onClick={(event) => { if (window.confirm(t('upload.confirm'))) onDelete(event) }}
              fluid={true}
              content={t('upload.delete')}
            />
          </div> :
          <Button
            className="send"
            primary
            onClick={onSubmit}
            fluid={true}
            content={t('upload.send')}
            disabled={name.trim().length == 0 || !pointCloud || (releaseType == 'group' && releasedUsers.length == 0)}
          />}
      </Segment>
    </Form>

  )

}
export default Upload;