import '@testing-library/jest-dom'
import * as React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import Upload from "./Upload"
import './../i18n';

describe('Show Upload', () => {

    test('render App Upload', () => {
        render(<Upload></Upload>)
        expect(screen.getByText('Upload')).toBeInTheDocument()
        expect(screen.getByText('Upload point cloud')).toBeInTheDocument()
    })

    test('no Upload without file', () => {
        render(<Upload></Upload>)
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Name/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Place/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Scan device/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Scan date/i), {
            target: { value: '12.12.2030' },
        })
        expect(screen.getByText(/upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Description/i), {
            target: { value: 'test' },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Point cloud access type/i), {
            target: { value: 'group' },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');
        fireEvent.change(screen.getByLabelText(/Users/i), {
            target: { value: ['test1234', 'test'] },
        })
        expect(screen.getByText(/Upload point cloud/i)).toHaveAttribute('disabled');

    })

    test('render App Edit', () => {
        const pointCloud = { "name": "test1", "place": "test2", "added_at": "2021-05-26T11:39:47.706339Z", "scan_date": "2021-05-12", "scan_device": "Leica BLK2GO", "description": "Test3", "visibility": "public", "metadata": 1 };
        render(<Upload pointCloud={pointCloud}></Upload>)
        expect(screen.getByText('Edit point cloud')).toBeInTheDocument()
        expect(screen.getByText('Save changes')).toBeInTheDocument()
        expect(screen.getByText('Test3')).toBeInTheDocument()
    })

});