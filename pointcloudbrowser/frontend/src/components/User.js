import React from 'react'
import { Header } from 'semantic-ui-react'
import { useTranslation } from "react-i18next";

function User(props) {
    const { t } = useTranslation();

    return (
        <div>
            <Header>
                <h2>{t('user.title')}</h2>
            </Header>
            <p>
                {t('user.username') + props.user}
            </p>
            <p>
                {t('user.email') + props.email}
            </p>
        </div>
    )
}

export default User