import React from 'react';
import { fromArrayBuffer } from 'geotiff';

class View2d extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }
    componentDidMount() {

        (async function (myRef, url, handleError) {
            try {
                const response = await fetch(url);
                const arrayBuffer = await response.arrayBuffer();
                const tiff = await fromArrayBuffer(arrayBuffer);
                const image = await tiff.getImage();
                const data = await image.readRasters();
                const datacopy = [...new Set(data[0])].sort(function (a, b) {
                    return a - b;
                });
                var secondMin = parseInt(datacopy[1]);
                var max = parseInt(datacopy[datacopy.length - 1]);

                const plot = new plotty.plot({
                    canvas: myRef,
                    data: data[0],
                    width: image.getWidth(),
                    height: image.getHeight(),
                    domain: [secondMin, max],
                    colorScale: "greys"
                });
                plot.render();
            }
            catch (e) {
                handleError({response: {detail: "Something went wrong with the 2d view . Please try again"}})

            }


        })(this.myRef.current, this.props.actualPointcloud.tifpath, this.props.handleError);
    }



    render() {
        return <canvas id="canvas" ref={this.myRef} />;
    }
}

export default View2d;