import jwt_decode from "jwt-decode";

export function getView2d(id) {
  if ('token' in localStorage) {
    checkToken();
    return getAuthenticatedJson(`api/pointclouds/${id}/view2d/`, localStorage.getItem('token')).then(parseJSON);
  }
  return getJson(`api/pointclouds/${id}/view2d/`).then(parseJSON);
}

export function getView(id) {
  if ('token' in localStorage) {
    checkToken();
    return getAuthenticatedJson(`api/pointclouds/${id}/view3d/`, localStorage.getItem('token')).then(parseJSON);
  }
  return getJson(`api/pointclouds/${id}/view3d/`).then(parseJSON);
}

export function deletePointCloud(id) {
  checkToken();
  return deleteAuthenticatedJson(`api/pointclouds/${id}/`, localStorage.getItem('token')).then(parseJSON);
}

export function editMetadata(metadata, id) {
  checkToken();
  return patchAuthenticatedJson(
    `api/pointclouds/${id}/`,
    localStorage.getItem('token'),
    metadata
  ).then(parseJSON);
}

export function getPointClouds(search, page) {
  if ('token' in localStorage) {
    checkToken();
    return getAuthenticatedJson(
      `/api/pointclouds?page=${page}&pageSize=25${(search != "" ? "&search=" + search : "")}`,
      localStorage.getItem('token')
    ).then(parseJSON);
  }
  return getJson(`/api/pointclouds?page=${page}&pageSize=25${(search != "" ? "&search=" + search : "")}`).then(parseJSON);
}

export function authenticate(login, password) {
  return postJson("login/token-auth/", { username: login, password: password }).then(parseJSON);
}

export function signup(login, password, email) {
  return postJson("login/users/", { username: login, password: password, email: email }).then(parseJSON);
}

export function getUser() {
  checkToken();
  return getAuthenticatedJson("login/current-user/", localStorage.getItem('token')).then(parseJSON);
}

export function sendPointCloud(metadata, pointCloud, oldPointCloud, callback, errorCallback, callbackReload) {
  checkToken();
  var data = metadata;
  if (oldPointCloud) {
    data = {
      name: metadata.name,
      description: metadata.description,
      place: metadata.place,
      scan_date: metadata.scann_date,
      scan_device: metadata.scan_device,
      visibility: metadata.visibility,
      users_to_share_with: metadata.users_to_share_with,
      old_pointcloud_id: oldPointCloud.id
    }
  }
  return sendRecursive(
    pointCloud,
    0, "api/upload/",
    data,
    localStorage.getItem('token'),
    callback,
    errorCallback,
    callbackReload)

}

function sendRecursive(file, start, endpoint, metadata, token, callback, errorCallback, callbackReload) {
  checkToken();
  var sliceSize = 1024 * 1024 * 16;

  if (file.size > start) {
    var end = (file.size - (start + sliceSize) < 0) ? file.size : start + sliceSize;
    var chunk = slice(file, start, end);
    var data = new FormData()
    data.append('file', chunk)
    data.append('filename', file.name)
    putChuncks(endpoint, token, data, start, end, file.size)
      .then((response) => response.json()).then(result => {
        sendRecursive(file, result.offset, result.url, metadata, token, callback, errorCallback, callbackReload);
        callback(parseInt(100 / file.size * result.offset))
      })
      .catch((error) => {
        callback(0);
        errorCallback(error);
      })
  } else {
    postChuncks(endpoint, token, metadata)
      .then((response) => {
        callback(0)
        callbackReload();
      })
      .catch((error) => {
        callback(0)
        errorCallback(error);
      })
  }
}

function getAuthenticatedJson(endpoint, token) {
  return fetch(`${endpoint}`, {
    method: "GET",
    headers: {
      Authorization: `JWT ${token}`,
      Accept: "application/json"
    }
  }).then(checkStatus);
}

function postJson(endpoint, params) {
  return fetch(`${endpoint}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify(params)
  }).then(checkStatus);
}

function patchAuthenticatedJson(endpoint, token, params) {
  return fetch(`${endpoint}`, {
    method: "PATCH",
    headers: {
      Authorization: `JWT ${token}`,
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify(params)
  }).then(checkStatus);
}

function deleteAuthenticatedJson(endpoint, token) {
  return fetch(`${endpoint}`, {
    method: "DELETE",
    headers: {
      Authorization: `JWT ${token}`,
      "Content-Type": "application/json",
      Accept: "application/json"
    },
  }).then(checkStatus);
}

function putChuncks(endpoint, token, data, start, end, totalFileSize) {
  return fetch(`${endpoint}`, {
    method: "PUT",
    headers: {
      'Content-Range': `bytes ${start}-${end - 1}/${totalFileSize}`,
      Authorization: `JWT ${token}`,
    },
    body: data
  }).then(checkStatus);
}

function postChuncks(endpoint, token, metadata) {
  return fetch(`${endpoint}`, {
    method: "POST",
    headers: {
      Authorization: `JWT ${token}`,
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify(metadata)
  }).then(checkStatus);
}

function checkStatus(response) {
  if ((response.status >= 200 && response.status < 300) || response.status == 304) {
    return response;
  } else if (400 || 401) {
    const error = new Error("RequestError");
    error.response = response;
    throw error;
  } else {
    const error = new Error(response.statusText);
    error.response = JSON.stringify({ detail: response.statusText })
    throw error;
  }
}

function parseJSON(response) {
  return response.json();
}

function getJson(endpoint) {
  return fetch(`${endpoint}`, {
    method: "GET",
    headers: {
      Accept: "application/json"
    },
  }).then(checkStatus);
}

function slice(file, start, end) {
  var sliceFunction = file.mozSlice ? file.mozSlice :
    file.webkitSlice ? file.webkitSlice :
      file.slice;
  return sliceFunction.bind(file)(start, end);
}

function checkToken() {
  if (jwt_decode(localStorage.getItem('token')).exp < (Date.now()) / 1000) {
    localStorage.removeItem('token')
    window.location.reload()
  }
  if (jwt_decode(localStorage.getItem('token')).exp < (Date.now() + 18000000) / 1000) {
    postJson("login/refresh-token/", { token: localStorage.getItem('token') })
      .then(parseJSON)
      .then((refresh) => {
        localStorage.setItem('token', refresh.token);
      })
      .catch(
        (error) => {
          localStorage.removeItem('token')
          window.location.reload()
        })
  }
}
