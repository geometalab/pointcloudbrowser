import i18n from "i18next";
import {initReactI18next} from "react-i18next";

const resources = {
    en: {
        translation: {
            "title": "Point Cloud Browser",
            "noPointClouds": "There were no point clouds loaded",
            "listButton": "List",
            "2dConfiguration": "2D",
            "3dConfiguration": "3D",
            "uploadButton": "Upload",
            "loading": "Loading",
            "uploadMessageHeader": "Just one second",
            "uploadMessage": "We are uploading your data. Progress: {{ uploadProgress }}/100",
            "processMessage": "The upload is done. We are now processing your point cloud. This process can take some minutes.",
            "viewMessageHeader":"Prepare View",
            "viewProcessMessage": "This point cloud isn't ready now. The process can take some minutes.",
            "logout": "Logout",
            "confirm": "Are you sure you want to log out?",
            "info": "Login first",
            list:{
                "title": "Point cloud list",
                "search": "Search",
                "placeholder": "No point clouds found",
                "error": "Something went wrong",
                "reload": "Load more"
            },
            listEntry:{
                "description": "Description: ",
                "uploadDate": "Uploaded on: ",
                "uploadUser": "Uploaded by: ",
                "captureDate": "Captured on: ",
                "release": "Access type: ",
                "download": "Download LAZ",
                "scannDevice": "Scan device: ",
                "place": "Scan place: "
            },
            login: {
                "title": "Log in",
                "login": "Login",
                "username": "Username",
                "usernameError": "Username should not contain spaces",
                "password": "Password",
                "repeatPassword": "Password repeat",
                "send": "Log-in",
                "save": "Register",
                "register": "No account yet",
                "back": "Log in with an existing account",
                "error": "Registration failed",
                "error2": "Unable to log in with provided credentials",
                "emailError": "Please enter a valid email address",
                "passwordError": "Password must have at least 12 characters",
                "repeatPasswordError": "Passwords must be the same"
            },
            upload: {
                "title": "Upload",
                "edit":"Edit point cloud",
                "name": "Name",
                "place": "Place",
                "description": "Description",
                "release": "Access Type",
                "users": "Users",
                "scannDate": "Scan date",
                "device": "Scan device",
                "file": "Choose a file",
                "releaseType": "Point cloud access type",
                "private": "Private: Visible only for me",
                "public": "Public: Visible for everyone",
                "group": "Group: Visible for selectet users",
                "inputError": "Enter at least one user",
                "send": "Upload point cloud",
                "sendError": "Please try again, there was a problem",
                "downloadOld": "Download LAZ to edit and upload the edited point cloud",
                "replaceOld": "Attention: The old point cloud will be overwritten without the possibility of restoring.",
                "editSave": "Save changes",
                "delete": "Delete pointcloud",
                "confirm": "Are you sure you want to delete this point cloud?"
            },
            about:{
                "about": "About",
                "description":"The point cloud browser allows you to share your point clouds with everyone, friends or co-workers. On the other hand you can look at all the point clouds you have access to. ",
                "howToUse": "How to use the point cloud browser",
                "descriptionUse": "You can use the point cloud browser as a guest user to have a quick look at all the public point clouds. Additionally if you registered and are logged in, you can upload your own data and view the point clouds where you have been added to.",
                "license": "License and Copyright",
                "descriptionLisence": "This Tool is under the BSD License. The copyright belongs to Denis Nauli and Nadine Sennhauser.",
                "resources": "Used resources",
                "source": "Source Code",
                "potree": "Potree",
                "pointcloudpipeline": "Pointcloudpipeline"
            },
            user: {
                "title": "Your Profile",
                "email": "Email: ",
                "username": "User: "
            },
            description : {
                "download" : "Download",
                "edit": "Edit"
            },
            configuration2d :{
                "informations": "GeoTIFF Informations",
                "calculate": "Calculation: The GeoTIFF is calculated from a bird's eye view and shows the highest value of the point cloud per pixel. The calculations are made using PDAL functions.",
                "usage": "Download use: The result is a one channel image with the height as values. A standard photo viewer like the Windows photo viewer cannot open this. The best way to use this GeoTIFF is to open it in GIS programs such as QGIS."
            },
            view2d: {
                "error": "Something went wrong with the 2d view. Please try again"
            }
        }
    }
}

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: "en",

        keySeparator: ".",

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;
