from api.models import EPTTempModel, TIFTempModel
from datetime import datetime, timedelta, timezone
import shutil
import os

def delete_old_ept():
    to_delte_ept = EPTTempModel.objects.filter(last_used_date__lt=(datetime.now(timezone.utc) - timedelta(hours=24)))

    for temp_model in to_delte_ept:
        if os.path.exists(temp_model.save_path):
            shutil.rmtree(temp_model.save_path)

    to_delte_ept.delete()

def delete_old_tif():
    to_delte_tif = TIFTempModel.objects.filter(last_used_date__lt=(datetime.now(timezone.utc) - timedelta(hours=24)))

    for temp_model in to_delte_tif:
        if os.path.exists(temp_model.save_path):
            os.remove(temp_model.save_path)
    
    to_delte_tif.delete()
