from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from tempdeleteprocess import deletetempprocess


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(deletetempprocess.delete_old_ept, 'interval', minutes=10)
    scheduler.add_job(deletetempprocess.delete_old_tif, 'interval', minutes=10)
    scheduler.start()
