from django.test import TestCase
from .models import User
from rest_framework import status

import json

class UserTestCreation(TestCase):
    def setUp(self):
        self.login_url = '/login/users/'

        self.valid_user_creation = {
            "username" : "denis",
            "password": "secret1234",
            "email": "denis@gmail.com"
        }
    
        self.invalid_user_creation_missing_email = {
            "username" : "denis2",
            "password": "secret1234"
        }
        
        self.invalid_user_creation_missing_password = {
            "username" : "denis",
            "email": "denis@gmail.com"
        }

        self.invalid_user_creation_missing_username = {
            "password": "secret1234",
            "email": "denis@gmail.com"
        }

        self.invalid_user_creation_invalid_email = {
            "username" : "denis",
            "password": "secret1234",
            "email": "denis"
        }
    
    def test_user_creation_valid(self):
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_user_already_exists(self):
        self.client.post(
            self.login_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data), "{'username': [ErrorDetail(string='A user with that username already exists.', code='unique')]}")

    def test_user_creation_invalid_missing_email(self):
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.invalid_user_creation_missing_email),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data), "{'email': [ErrorDetail(string='This field is required.', code='required')]}")

    def test_user_creation_invalid_missing_password(self):
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.invalid_user_creation_missing_password),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data), "{'password': [ErrorDetail(string='This field is required.', code='required')]}")

    def test_user_creation_invalid_missing_username(self):
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.invalid_user_creation_missing_username),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data), "{'username': [ErrorDetail(string='This field is required.', code='required')]}")
    
    def test_user_creation_invalid_email(self):
        response = self.client.post(
            self.login_url,
            data = json.dumps(self.invalid_user_creation_invalid_email),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data), "{'email': [ErrorDetail(string='Enter a valid email address.', code='invalid')]}")

class UserTestTokenAuth(TestCase):
    def setUp(self):
        self.create_url = '/login/users/'
        self.login_url = '/login/token-auth/'

        self.valid_user_creation = {
            "username" : "denis",
            "password": "secret1234",
            "email": "denis@gmail.com"
        }

        self.valid_login = {
            "username" : "denis",
            "password": "secret1234"
        }

        self.invalid_login = {
            "username" : "denis2",
            "password": "secret1234"
        }

    def test_get_token(self):
        self.client.post(
            self.create_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )

        response = self.client.post(
            self.login_url,
            data = json.dumps(self.valid_login),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)

    def test_get_token_invalid(self):
        self.client.post(
            self.create_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )

        response = self.client.post(
            self.login_url,
            data = json.dumps(self.invalid_login),
            content_type = 'application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse('token' in response.data)

class UserTestTokenAuth(TestCase):
    def setUp(self):
        self.create_url = '/login/users/'
        self.current_url = '/login/current-user/'

        self.valid_user_creation = {
            "username" : "denis",
            "password": "secret1234",
            "email": "denis@gmail.com"
        }

    def test_get_userinfo_from_token(self):
        response_with_token = self.client.post(
            self.create_url,
            data = json.dumps(self.valid_user_creation),
            content_type = 'application/json'
        )

        token = 'JWT '+response_with_token.data.get('token')

        response = self.client.get(
            self.current_url,
            HTTP_AUTHORIZATION=token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'username': 'denis', 'email': 'denis@gmail.com'})

    
    def test_get_userinfo_from_token_unauthorized(self):
        response = self.client.get(self.current_url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)