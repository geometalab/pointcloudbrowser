from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets, permissions, status
from .serializers import UserSerializer, UserSerializerWithToken

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

@swagger_auto_schema(
    method='get',
    operation_description="Create a user.",
    responses={
        "200": openapi.Response(
            description='Response when token valid.',
            schema=openapi.Schema(
                type=openapi.TYPE_OBJECT, 
                properties={
                    'username': openapi.Schema(type=openapi.TYPE_STRING, description='Your username'),
                    'email': openapi.Schema(type=openapi.TYPE_STRING, description='Your email')
                }
            ),
            examples={
                "application/json": {
                    "username": "max",
                    "email": "example@mail.com"
                }
            }
        )
    }
)
@api_view(['GET'])
def current_user(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)


class UserList(APIView):
    permission_classes = (permissions.AllowAny,)

    @swagger_auto_schema(
        operation_description="Create a user.",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT, 
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description='Set a username'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='Set a password'),
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='Set an email'),
            }
        ), 
        responses={
            "201": openapi.Response(
                description='Response when successfully created.',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT, 
                    properties={
                        'token': openapi.Schema(type=openapi.TYPE_STRING, description='Your token'),
                        'username': openapi.Schema(type=openapi.TYPE_STRING, description='Your username'),
                        'email': openapi.Schema(type=openapi.TYPE_STRING, description='Your email')
                    }
                ),
                examples={
                    "application/json": {
                        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3LCJ1c2VybmFtZSI6ImRvbm90ZGVjb2RldGhpcyIsImV4cCI6MTYyMDc2NTYxMSwiZW1haWwiOiJzZWNyZXRAc2VjcmV0bWFpbC5jb20ifQ._qT7wujb4IlLw-VnGWMWcpQ4N8jBnIyQ8aDYwqSnxVA",
                        "username": "max",
                        "email": "example@mail.com"
                    }
                }
            )
        }
    )
    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)